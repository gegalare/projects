#include "sock_wrap.h"
#include "table.h"
#include "intr.h"
int main(void)
{
	try
	{
		server serv;
		serv.start();
		serv.trade();
	} 
    catch (Sock_exception & e)
    {
	    e.report();
    }
    catch (Table_exception & e)
    {
	    e.report();
    }
    catch (Intr_exception & e)
    {
        e.report();
    }
	return 0;
}
