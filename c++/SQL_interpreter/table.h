#ifndef __TABLE_H__
#define __TABLE_H__
#include <unistd.h>
#include <iostream>
#include <stdexcept>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <vector>
#include <regex>

using namespace std;

#define MAX_FIELD_NAME_LEN 20

class Table_exception
{
private:
    int err_code;

public:
    Table_exception(int err) : err_code(err) {}
    enum Table_exception_code
    {
        CREATE_1,
        CREATE_2,
        CREATE_3,
        CREATE_4,
        SELECT_1,
        SELECT_2,
        DROP,
        INSERT_1,
        INSERT_2,
        INSERT_3,
        UPDATE_1,
        UPDATE_2,
        UPDATE_3,
        LIKE
    };
    void report();
};

int ToWorkWithoutWarnings;

void MakeStrGreatAgain(string &str);
void drop(string fileName);

enum FieldType
{
    TEXT,
    LONG,
    ZERO,
    OP,
    OP_CMP,
    NUM,
    STR,
    ERR,
    NOT,
    AND,
    OR
};

struct FieldData
{
    enum FieldType type;
    string fieldName;
    long number = 0;
};

struct TableInfo
{
    long data_offset = 0;         //длина строки
    long field_count = 0;         //количество столбцов
    long record_count = 0;        //количество строк
    long first_record_offset = 0; //адрес начала первой строки таблицы
    long last_record_offset = 0;  //адрес начала последней строки таблицы
};

struct FieldInfo
{
    char fieldName[MAX_FIELD_NAME_LEN] = {'\0'}; //название столбца
    enum FieldType type;                         //тип ячеек
    long length;                                 //длина ячейки
};

class Table
{
private:
    FILE *file;
    struct TableInfo info;
    vector <struct FieldInfo> vFields;
public:
    Table(string tfileName, vector<struct FieldInfo> tFieldInfo);
    ~Table();
    void select(vector <string> sFieldName, vector <long> sDataOffset);
    void update(struct FieldData fieldName, vector <struct FieldData> uFieldData, vector <long> uDataOffset);
    void insert(vector <struct FieldData> iFieldData);
    void deleteField(vector <long> dDataOffset);
    void writeTable();
    enum FieldType findField(string nameOfField);
    vector <long> like(string fieldName, string Field, int flag);
    vector <long> all(string fieldName);
    vector <long> in(vector <long> toChangeLong, vector <string> toChangeStr,
                     vector <struct FieldData> afterIN, enum FieldType state, int flag);
    vector <long> boolean(vector <struct FieldData> log_poliz);
    enum FieldType poliz_analize(vector <long> &toChangeLong, vector <string> &toChangeStr, vector <struct FieldData> poliz);

     
};

Table :: Table(string tfileName, vector <struct FieldInfo> tFieldInfo)
{
    if (tfileName == "SELECT")
    {
        file = tmpfile();
        if(!(file))
        {
            throw Table_exception(Table_exception :: CREATE_1);
        }
        vFields = tFieldInfo;
        info.field_count = tFieldInfo.size();
        for (int i = 0; i < info.field_count; i++)
        {
            info.data_offset += tFieldInfo[i].length;
        }
        info.first_record_offset = sizeof(struct TableInfo) + info.field_count * sizeof(struct FieldInfo);
        info.last_record_offset = info.first_record_offset;
        fwrite(&info, sizeof(struct TableInfo), 1, file);
        for (int i = 0; i < info.field_count; i++)
        {
           fwrite(&tFieldInfo[i], sizeof(struct FieldInfo), 1, file);
        }
    }
    else if (tFieldInfo.empty())
    { //открываем уже существующий файл
        if ((file = fopen(tfileName.c_str(), "r+b")) == NULL)
        {
            throw Table_exception(Table_exception :: CREATE_2);
        }
        fread(&info, sizeof(struct TableInfo), 1, file);

        for (int i = 0; i < info.field_count; i++)
        {
            struct FieldInfo tmpFieldInfo;
            fread(&tmpFieldInfo, sizeof(struct FieldInfo), 1, file);
            vFields.push_back(tmpFieldInfo);
        }
    }
    else
    { //создаем новый файл
        if ((file = fopen(tfileName.c_str(), "r+b")) != NULL)
        {
            throw Table_exception(Table_exception :: CREATE_3);
        }
        if ((file = fopen(tfileName.c_str(), "a+b")) == NULL)
        {
            throw Table_exception(Table_exception :: CREATE_4);
        }
        vFields = tFieldInfo;
        info.field_count = tFieldInfo.size();
        for (int i = 0; i < info.field_count; i++)
        {
            info.data_offset += tFieldInfo[i].length;
        }
        info.first_record_offset = sizeof(struct TableInfo) + info.field_count * sizeof(struct FieldInfo);
        info.last_record_offset = info.first_record_offset;
        fwrite(&info, sizeof(struct TableInfo), 1, file);
        for (int i = 0; i < info.field_count; i++)
        {
            fwrite(&tFieldInfo[i], sizeof(struct FieldInfo), 1, file);
        }
    }
}

Table :: ~Table()
{
    fseek(file, 0, SEEK_SET);
    fwrite(&info, sizeof(struct TableInfo), 1, file);
    fclose(file);
}

void Table :: writeTable()
{
    cout << endl;
    for (int i = 0; i < info.field_count; i++)
    {
        cout << vFields[i].fieldName;
        int tmp = (strlen(vFields[i].fieldName) - vFields[i].length);
        if (tmp <= 0)
        {
            for (int j = strlen(vFields[i].fieldName) + 1; j <= vFields[i].length; j++)
            {
                cout << " ";
            }
        }
        else
        {
            cout << " ";
        } 
    }
    cout << endl;
    fseek(file, info.first_record_offset, SEEK_SET);
    for (int i = 0; i < info.record_count; i++)
    {
        for (int j = 0; j < info.field_count; j++)
        {
            if (vFields[j].type == TEXT)
            {
                char *tmp = new char[vFields[j].length + 1];
                for (int k = 0; k < vFields[j].length; k++)
                {
                    tmp[k] = ' ';
                }
                tmp[vFields[j].length] = '\0';
                fread(tmp, vFields[j].length, 1, file);
                cout << tmp;
                delete []tmp;
            }
            else
            {
                long tmp;
                fread(&tmp, sizeof(long), 1, file);
                cout << tmp;
                int tmpSize;
                for (tmpSize = 0; tmp > 0; tmpSize++)
                {
                    tmp /= 10;
                }

                for (int k = tmpSize; k < (ToWorkWithoutWarnings = sizeof(long)); k++)
                {
                    cout << " ";
                }
            }
        }
        cout << endl;
    }
    cout << endl;
}

void drop(string fileName)
{
    if (unlink(fileName.c_str()) != 0)
    {
        throw Table_exception(Table_exception :: DROP);
    }
    cout << "Table dropped" << endl << endl;
}

void Table :: insert (vector <struct FieldData> iFieldData)
{
    if ((ToWorkWithoutWarnings = iFieldData.size()) != info.field_count)
    {
        throw Table_exception(Table_exception :: INSERT_1);
    }
    if (info.record_count == 0)
    {
        fseek(file, info.first_record_offset, SEEK_SET);
    }
    else
    {
        fseek(file, info.last_record_offset + info.data_offset, SEEK_SET);
    }
    for (int i = 0; i < info.field_count; i++)
    {
        if (iFieldData[i].type != vFields[i].type)
        {
            throw Table_exception(Table_exception :: INSERT_2);
        }
        if (iFieldData[i].type == TEXT)
        {
            if ((ToWorkWithoutWarnings = (iFieldData[i].fieldName).length()) > vFields[i].length)
            {
                throw Table_exception(Table_exception :: INSERT_3);
            }
            char *tmp = new char[vFields[i].length + 1];
            int k = (iFieldData[i].fieldName).length();
            strcpy(tmp, (iFieldData[i].fieldName).c_str());
            for (int j = k; j < vFields[i].length; j++)
            {
                tmp[j] = ' ';
            }
            tmp[vFields[i].length] = '\0';
            fwrite(tmp, vFields[i].length, 1, file);
            delete[] tmp;
        }
        else
        {   
            fwrite(&iFieldData[i].number, vFields[i].length, 1, file);
        }
    }
    info.record_count++;
    if (info.record_count != 1)
    {
        info.last_record_offset += info.data_offset;
    }
}

void Table :: update (struct FieldData fieldName, vector <struct FieldData> poliz, vector <long> uDataOffset)
{   
    vector <long> toChangeLong;
    vector <string> toChangeStr;
    long fieldLength, fieldOffset = 0;

    for (int i = 0; i < info.field_count; i++)
    {
        if (vFields[i].fieldName == fieldName.fieldName)
        {
            if (vFields[i].type == fieldName.type)
            {
                fieldLength = vFields[i].length;
                break;
            }
            else
            {
                throw Table_exception(Table_exception :: UPDATE_1);
            }
        }
        fieldOffset += vFields[i].length;
    }

    if (fieldName.type == TEXT)
    {
        if (poliz[0].type == TEXT)
        {
            long tmp_length, offset = 0;
            for (int i = 0; i < info.field_count; i++)
            {
                if (vFields[i].fieldName == poliz[0].fieldName)
                {
                    tmp_length = vFields[i].length;
                    break; 
                }
                else 
                {
                    offset += vFields[i].length;
                }
            }

            if (fieldLength < tmp_length)
            {
                throw Table_exception(Table_exception :: UPDATE_2);
            }

            int size = uDataOffset.size();
            
            for (int i = 0; i < size; i++)
            {
                fseek(file, uDataOffset[i] + offset, SEEK_SET);
                char *tmp = new char[fieldLength + 1];
                fread(tmp, tmp_length, 1, file);
                for (int j = tmp_length; j < fieldLength; j++)
                {
                    tmp[j] = ' ';
                }
                tmp[fieldLength] = '\0';
                string tmp_str = tmp;
                toChangeStr.push_back(tmp_str);
                delete[] tmp;
            }
        }
        else //there is string 
        {
            int size = uDataOffset.size();
            int check = poliz[0].fieldName.size();
            if (fieldLength < check)
            {
                throw Table_exception(Table_exception :: UPDATE_3);
            }

            char *tmp = new char[fieldLength + 1];
            strcpy(tmp, poliz[0].fieldName.c_str());
            for (int i = check; i < fieldLength; i++)
            {
                tmp[i] = ' ';
            }
	        tmp[fieldLength] = '\0';
            string tmp_str = tmp;
            for (int i = 0; i < size; i++)
            {
                toChangeStr.push_back(tmp_str);
            }
            delete []tmp;
        }
    }
    else 
    {
        vector <long> tmpLongVector;
        int size = poliz.size();
        int offsetSize = uDataOffset.size();
        for (int m = 0; m < offsetSize; m++)
        {
            tmpLongVector.clear();
            long tmp_length, offset = 0, help = 0;
            for (int i = 0; i < size; i++)
            {
                if (poliz[i].type == LONG)
                {
                    for (int j = 0; j < info.field_count; j++)
                    {
                        if (vFields[j].fieldName == poliz[i].fieldName)
                        {
                            tmp_length = vFields[j].length;
                            break;
                        }
                        else 
                        {
                            offset += vFields[j].length;
                        }
                    }

                    fseek(file, uDataOffset[m] + offset, SEEK_SET);
                    long tmp;
                    fread(&tmp, tmp_length, 1, file);
                    tmpLongVector.push_back(tmp);
                }
                else if (poliz[i].type == NUM)
                {
                    tmpLongVector.push_back(poliz[i].number);
                }
                else if(poliz[i].type == OP)
                {

                    if (poliz[i].fieldName == "+")
                    {
                        long value = tmpLongVector[i - help - 2] + tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "-")
                    {
                        long value = tmpLongVector[i - help - 2] - tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "/")
                    {
                        long value = tmpLongVector[i - help - 2] / tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "%")
                    {
                        long value = tmpLongVector[i - help - 2] % tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "*")
                    {
                        long value = tmpLongVector[i - help - 2] * tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    help += 2;
                }
            }
            toChangeLong.push_back(tmpLongVector[0]);
        }
    }

    for (int i = 0; i < (ToWorkWithoutWarnings = uDataOffset.size()); i++)
    {
        fseek(file, uDataOffset[i] + fieldOffset, SEEK_SET);
        if (fieldName.type == TEXT)
        {   
            char *tmp = new char[fieldLength + 1];
            strcpy(tmp, (toChangeStr[i]).c_str());
            fwrite(tmp, fieldLength, 1, file);
            delete[] tmp;
        }
        else
        {
            fwrite(&toChangeLong[i], fieldLength, 1, file);
        }
    }
}

void Table :: deleteField (vector <long> dDataOffset)
{
    sort(dDataOffset.rbegin(), dDataOffset.rend());
    for (int i = 0; i < (ToWorkWithoutWarnings = dDataOffset.size()); i++)
    {
        if (dDataOffset[i] == info.last_record_offset)
        {
            info.record_count--;
            info.last_record_offset -= info.data_offset;
        }
        else
        {
            int record_number = ((dDataOffset[i] - info.first_record_offset) / info.data_offset) + 1;
            int iterations_count = info.record_count - record_number;
            for (int iteration = 1; iteration <= iterations_count; iteration++)
            {
                fseek(file, dDataOffset[i] + iteration * info.data_offset, SEEK_SET);
                char *tmp = new char[info.data_offset];
                fread(tmp, info.data_offset, 1, file);
                fseek(file, dDataOffset[i] + (iteration - 1) * info.data_offset, SEEK_SET);
                fwrite(tmp, info.data_offset, 1, file);
                delete[] tmp;
            }
            info.record_count--;
            info.last_record_offset -= info.data_offset;
        }
    }
}

/*В функцию передаются вектор имен полей и вектор с адресами данных в файле.
Сначала по файлу ищутся столбцы, они записываются в вектор,
потом создается новый файл, туда пихаются эти столбцы. Далее нахожу по
исходному файлу конкретные записи, копирую их в временный файл.
Вывожу временный файл, удаляю его. */
void Table :: select (vector <string> sFieldName, vector <long> sDataOffset)
{   
    vector <struct FieldInfo> sVec;
    vector <long> vFieldOffset;
    if (sFieldName.empty())
    {
	    int tmp = vFields.size();
        for (int i = 0; i < tmp; i++)
        {
            string tmp = vFields[i].fieldName;
            sFieldName.push_back(tmp);
        }    
    }

    if (vFields.size() < sFieldName.size())
    {
        throw Table_exception(Table_exception :: SELECT_1);
    }

    for (int i = 0; i < (ToWorkWithoutWarnings = sFieldName.size()); i++)
    {
        long fieldOffset = 0;
        int helpfullTMP;
        for (int j = 0; j < (helpfullTMP = vFields.size()); j++)
        {
            if (strcmp(vFields[j].fieldName, sFieldName[i].c_str()) == 0)
            {
                sVec.push_back(vFields[j]);
                break;
            }
            fieldOffset += vFields[j].length;
        }
        vFieldOffset.push_back(fieldOffset);
    }

    if (sVec.size() != sFieldName.size())
    {
        throw Table_exception(Table_exception :: SELECT_2);
    }

    Table tmpTab("SELECT", sVec);
    for (int i = 0; i < (ToWorkWithoutWarnings = sDataOffset.size()); i++)
    {
        vector <struct FieldData> sVecData;
        for (int j = 0; j < ( ToWorkWithoutWarnings = (tmpTab.vFields).size()); j++)
        {
            struct FieldData tmpFieldData;
            tmpFieldData.type = tmpTab.vFields[j].type;
            fseek(file, sDataOffset[i] + vFieldOffset[j], SEEK_SET);
            if (tmpFieldData.type == TEXT)
            {
                char *tmp = new char[tmpTab.vFields[j].length + 1];
                fread(tmp, tmpTab.vFields[j].length, 1, file);
                tmp[tmpTab.vFields[j].length] = '\0';
                string tmpStr(tmp);
                tmpFieldData.fieldName = tmpStr;
                delete[] tmp;
            }
            else
            {
               fread(&tmpFieldData.number, tmpTab.vFields[j].length, 1, file);
            }
            sVecData.push_back(tmpFieldData);
        }
        tmpTab.insert(sVecData);
    }
    tmpTab.writeTable();
}


vector <long> Table :: like(string fieldName, string Field, int flag)
{
    vector <long> vRecordOffset;
    long lengthOfField, fieldOffset = 0;
    int helpfullTMP;
    for (int j = 0; j < (helpfullTMP = vFields.size()); j++)
    {
        if (strcmp(vFields[j].fieldName, fieldName.c_str()) == 0)
        {
		    lengthOfField = vFields[j].length;
            if (vFields[j].type != TEXT)
            {
                throw Table_exception(Table_exception :: LIKE);
            }
            break;
        }
    fieldOffset += vFields[j].length;
    }
    for (int j = 0; j < ( ToWorkWithoutWarnings = info.record_count); j++)
    {
        fseek(file, info.first_record_offset +
                fieldOffset + info.data_offset * j, SEEK_SET);
        char *tmp = new char[lengthOfField + 1];
        fread(tmp, lengthOfField, 1, file);
        tmp[lengthOfField] = '\0';
        regex regular(Field);
        string strToCheck(tmp);
        int tmpI = strToCheck.length();
        for(int i = 0; i < tmpI; i++) 
        {
            if(strToCheck[i] == ' ') 
            { 
                    strToCheck.erase(i,1);
                    i--;
            }
        }
        if ((regex_match(strToCheck.begin(), strToCheck.end(), regular)) && (flag == 0))
        {
            long tmpL;
            fseek(file, -lengthOfField - fieldOffset, SEEK_CUR);
            tmpL = ftell(file);
            vRecordOffset.push_back(tmpL);
        }
	    else if (!(regex_match(strToCheck.begin(), strToCheck.end(), regular)) && (flag == 1))
	    {
            long tmpL;
            fseek(file, -lengthOfField - fieldOffset, SEEK_CUR);
            tmpL = ftell(file);
            vRecordOffset.push_back(tmpL);
	    }
        delete[] tmp;  
    }

    return vRecordOffset;
}

vector <long> Table :: all(string fieldName)
{
    vector <long> vRecordOffset;
    for (int j = 0; j < (ToWorkWithoutWarnings = info.record_count); j++)
    {
        fseek(file, info.first_record_offset + info.data_offset * j, SEEK_SET);
        long tmpL;
        tmpL = ftell(file);
        vRecordOffset.push_back(tmpL); 
    }
    return vRecordOffset;
}

vector <long> Table :: in(vector <long> toChangeLong, vector <string> toChangeStr,
                     vector <struct FieldData> afterIn, enum FieldType state, int flag)
{
    vector <long> vRecordOffset;
    if (state == TEXT)
    {
        
        int size = toChangeStr.size();
        int iSize = afterIn.size();
        for (int i = 0; i < size; i++)
        {
            int iflag = 0;
            for (int j = 0; j < iSize; j++)
            {
                if (toChangeStr[i] == afterIn[j].fieldName)
                {
                    iflag = 1;
                }
            }
            
            if (iflag == 1 && flag == 0)
            {
                long offset = info.first_record_offset + info.data_offset * i;
                vRecordOffset.push_back(offset);
            }
            else if (iflag == 0 && flag == 1)
            {
                long offset = info.first_record_offset + info.data_offset * i;
                vRecordOffset.push_back(offset);
            }
        }
    }
    else if (state == LONG)
    {
        int size = toChangeLong.size();
        int iSize = afterIn.size();
        for (int i = 0; i < size; i++)
        {
            int iflag = 0;
            for (int j = 0; j < iSize; j++)
            {
                if (toChangeLong[i] == afterIn[j].number)
                {
                    iflag = 1;
                }
            }

            if (iflag == 1 && flag == 0)
            {
                long offset = info.first_record_offset + info.data_offset * i;
                vRecordOffset.push_back(offset);
            }
            else if (iflag == 0 && flag == 1)
            {
                long offset = info.first_record_offset + info.data_offset * i;
                vRecordOffset.push_back(offset);
            }
        }
    }  
    return vRecordOffset;
}

vector <long> Table :: boolean(vector <struct FieldData> log_poliz)
{
    vector <long> vRecordOffset;
    vector <bool> results;
    vector <long> toChangeLong;
    vector <string> toChangeStr;
    enum FieldType state;
    int size = log_poliz.size();
    
    for (int i = 0; i < info.record_count; i++)
    {
        vector <bool> vecState;
        int checkSize = 0, help = 0;
        while (checkSize < size)
        {
            if(log_poliz[checkSize].type== TEXT)
            {
                state = TEXT;
                long tmp_length, offset = 0;
                for (int j = 0; j < info.field_count; j++)
                {
                    if (vFields[j].fieldName == log_poliz[checkSize].fieldName)
                    {
                        tmp_length = vFields[j].length;
                        break; 
                    }
                    else 
                    {
                        offset += vFields[j].length;
                    }
                }

                fseek(file, info.first_record_offset + info.data_offset * i + offset, SEEK_SET);
                char *tmp = new char[tmp_length + 1];
                fread(tmp, tmp_length, 1, file);
                tmp[tmp_length] = '\0';
                string tmp_str = tmp;
                int length = tmp_str.length();

                for(int k = 0; k < length; k++) 
                {
                    if(tmp_str[k] == ' ') 
                    { 
                            tmp_str.erase(k,1);
                            k--;
                    }
                }
                toChangeStr.push_back(tmp_str);
                delete[] tmp;
            }
            else if (log_poliz[checkSize].type == LONG)
            {
                state = LONG;
                long tmp_length, offset = 0;
                for (int j = 0; j < info.field_count; j++)
                {
                    if (vFields[j].fieldName == log_poliz[checkSize].fieldName)
                    {
                        tmp_length = vFields[j].length;
                        break; 
                    }
                    else 
                    {
                        offset += vFields[j].length;
                    }
                }

                fseek(file, info.first_record_offset + info.data_offset * i + offset, SEEK_SET);
                long tmp;
                fread(&tmp, tmp_length, 1, file);
                toChangeLong.push_back(tmp);
            }
            else if (log_poliz[checkSize].type== STR)
            {
                state = TEXT;
                toChangeStr.push_back(log_poliz[checkSize].fieldName);
            }
            else if (log_poliz[checkSize].type== NUM)
            {
                state = LONG;
                toChangeLong.push_back(log_poliz[checkSize].number);
            }
            else if (log_poliz[checkSize].type == OP)
            {
                if (log_poliz[checkSize].fieldName == "+")
                {
                    long value = toChangeLong[checkSize - help - 2] + toChangeLong[checkSize - help - 1];
                    toChangeLong.erase(toChangeLong.begin() + checkSize - help - 2, toChangeLong.begin() + checkSize - help);
                    toChangeLong.push_back(value);
                }
                else if (log_poliz[checkSize].fieldName == "-")
                {
                    long value = toChangeLong[checkSize - help - 2] - toChangeLong[checkSize - help - 1];
                    toChangeLong.erase(toChangeLong.begin() + checkSize - help - 2, toChangeLong.begin() + checkSize - help);
                    toChangeLong.push_back(value);
                }
                else if (log_poliz[checkSize].fieldName == "/")
                {
                    long value = toChangeLong[checkSize - help - 2] / toChangeLong[checkSize - help - 1];
                    toChangeLong.erase(toChangeLong.begin() + checkSize - help - 2, toChangeLong.begin() + checkSize - help);
                    toChangeLong.push_back(value);
                }
                else if (log_poliz[checkSize].fieldName == "%")
                {
                    long value = toChangeLong[checkSize - help - 2] % toChangeLong[checkSize - help - 1];
                    toChangeLong.erase(toChangeLong.begin() + checkSize - help - 2, toChangeLong.begin() + checkSize - help);
                    toChangeLong.push_back(value);
                }
                else if (log_poliz[checkSize].fieldName == "*")
                {
                    long value = toChangeLong[checkSize - help - 2] * toChangeLong[checkSize - help - 1];
                    toChangeLong.erase(toChangeLong.begin() + checkSize - help - 2, toChangeLong.begin() + checkSize - help);
                    toChangeLong.push_back(value);
                }
                help += 2;
            }
            else if (log_poliz[checkSize].type == OP_CMP)
            {
                if (log_poliz[checkSize].fieldName == "=")
                {
                    if (state == LONG)
                    {
                        if (toChangeLong[0] == toChangeLong[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        }                        
                    }
                    else
                    {
                        if (toChangeStr[0] == toChangeStr[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        } 
                    }
                    toChangeLong.clear();
                    toChangeStr.clear();
                    help = 0;
                }
                else if (log_poliz[checkSize].fieldName == "!=")
                {
                    if (state == LONG)
                    {
                        if (toChangeLong[0] != toChangeLong[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        }                        
                    }
                    else
                    {
                        if (toChangeStr[0] != toChangeStr[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        } 
                    }
                    toChangeLong.clear();
                    toChangeStr.clear();
                    help = 0;
                }
                else if (log_poliz[checkSize].fieldName == ">")
                {
                    if (state == LONG)
                    {
                        if (toChangeLong[0] > toChangeLong[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        }                       
                    }
                    else
                    {
                        if (toChangeStr[0] > toChangeStr[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        }
                    }
                    toChangeLong.clear();
                    toChangeStr.clear();
                    help = 0;
                }
                else if (log_poliz[checkSize].fieldName == ">=")
                {
                    if (state == LONG)
                    {
                        if (toChangeLong[0] >= toChangeLong[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        }                        
                    }
                    else
                    {
                        if (toChangeStr[0] >= toChangeStr[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        } 
                    }
                    toChangeLong.clear();
                    toChangeStr.clear();
                    help = 0;
                }
                else if (log_poliz[checkSize].fieldName == "<")
                {
                    if (state == LONG)
                    {
                        if (toChangeLong[0] < toChangeLong[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        }                        
                    }
                    else
                    {
                        if (toChangeStr[0] < toChangeStr[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        } 
                    }
                    toChangeLong.clear();
                    toChangeStr.clear();
                    help = 0;
                }
                else if (log_poliz[checkSize].fieldName == "<=")
                {
                    if (state == LONG)
                    {
                        if (toChangeLong[0] <= toChangeLong[1])
                        {
 
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        }                        
                    }
                    else
                    {
                        if (toChangeStr[0] <= toChangeStr[1])
                        {
                            vecState.push_back(true);
                        }
                        else
                        {
                            vecState.push_back(false);
                        } 
                    }
                    toChangeLong.clear();
                    toChangeStr.clear();
                    help = 0;
                }
            }
            else if (log_poliz[checkSize].type == OR)
            {
                int sizeVecState = vecState.size();
                bool value = vecState[sizeVecState - 2] or vecState[sizeVecState - 1];
                vecState.erase(vecState.begin() + sizeVecState - 2, vecState.begin() + sizeVecState);
                vecState.push_back(value);
            }
            else if (log_poliz[checkSize].type == AND)
            {
                int sizeVecState = vecState.size();
                bool value = vecState[sizeVecState - 2] and vecState[sizeVecState - 1];
                vecState.erase(vecState.begin() + sizeVecState - 2, vecState.begin() + sizeVecState);
                vecState.push_back(value);
            }
            else if (log_poliz[checkSize].type == NOT)
            {
                int sizeVecState = vecState.size();
                vecState[sizeVecState - 1] = not vecState[sizeVecState - 1];
            }
            checkSize++;
        }
        results.push_back(vecState[0]);
    }

    for (int i = 0; i < info.record_count; i++)
    {
        if(results[i] == true)
        {
            vRecordOffset.push_back(info.first_record_offset + info.data_offset * i);

        }
    }
    return vRecordOffset;
}

enum FieldType Table :: findField(string nameOfField)
{
    int count = vFields.size();
    for (int i = 0; i < count; i++)
    {
        if(strcmp(vFields[i].fieldName, nameOfField.c_str()) == 0)
        {
            return vFields[i].type;
       	}
    }
    return ZERO;
}

enum FieldType Table :: poliz_analize(vector <long> &toChangeLong, vector <string> &toChangeStr, vector <struct FieldData> poliz)
{
    enum FieldType state;
    if (poliz[0].type == TEXT)
    {
        state = TEXT;
        long tmp_length, offset = 0;
        for (int i = 0; i < info.field_count; i++)
        {
            if (vFields[i].fieldName == poliz[0].fieldName)
            {
                tmp_length = vFields[i].length;
                break; 
            }
            else 
            {
                offset += vFields[i].length;
            }
        }

        for (int i = 0; i < info.record_count; i++)
        {
            fseek(file, info.first_record_offset + info.data_offset * i + offset, SEEK_SET);
            char *tmp = new char[tmp_length + 1];
            fread(tmp, tmp_length, 1, file);
            tmp[tmp_length] = '\0';
            string tmp_str = tmp;
            int length = tmp_str.length();
            for(int i = 0; i < length; i++) 
            {
                if(tmp_str[i] == ' ') 
                { 
                        tmp_str.erase(i,1);
                        i--;
                }
            }
            toChangeStr.push_back(tmp_str);
            delete[] tmp;
        }
    }
    else if (poliz[0].type == STR)
    {
        state = STR;
        toChangeStr.push_back(poliz[0].fieldName);
    }
    else if ((poliz[0].type == NUM) || (poliz[0].type == LONG))
    {
        state = LONG;
        vector <long> tmpLongVector;
        int size = poliz.size();
        for (int m = 0; m < info.record_count; m++)
        {
            tmpLongVector.clear();
            long tmp_length, offset = 0, help = 0;
            for (int i = 0; i < size; i++)
            {
                if (poliz[i].type == LONG)
                {
                    for (int j = 0; j < info.field_count; j++)
                    {
                        if (vFields[j].fieldName == poliz[i].fieldName)
                        {
                            tmp_length = vFields[j].length;
                            break;
                        }
                        else 
                        {
                            offset += vFields[j].length;
                        }
                    }

                    fseek(file, info.first_record_offset + info.data_offset * m + offset, SEEK_SET);
                    long tmp;
                    fread(&tmp, tmp_length, 1, file);
                    tmpLongVector.push_back(tmp);
                }
                else if (poliz[i].type == NUM)
                {
                    tmpLongVector.push_back(poliz[i].number);
                }
                else if(poliz[i].type == OP)
                {
                    if (poliz[i].fieldName == "+")
                    {
                        long value = tmpLongVector[i - help - 2] + tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "-")
                    {
                        long value = tmpLongVector[i - help - 2] - tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "/")
                    {
                        long value = tmpLongVector[i - help - 2] / tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "%")
                    {
                        long value = tmpLongVector[i - help - 2] % tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    else if (poliz[i].fieldName == "*")
                    {
                        long value = tmpLongVector[i - help - 2] * tmpLongVector[i - help - 1];
                        tmpLongVector.erase(tmpLongVector.begin() + i - help - 2, tmpLongVector.begin() + i - help);
                        tmpLongVector.push_back(value);
                    }
                    help += 2;
                }
            }
            toChangeLong.push_back(tmpLongVector[0]);
        }
    }
    return state;
}

void Table_exception :: report()
{
    switch (err_code)
    {
    case CREATE_1:
        cerr << "ERROR CODE TABLE: Can't create tmp file for SELECT" << endl << endl;
        break;

    case CREATE_2:
        cerr << "ERROR CODE TABLE: Can't open file with that name. Is it exists?" << endl << endl;
        break;

    case CREATE_3:
        cerr << "ERROR CODE TABLE: File with that name already exist" << endl << endl;
        break;

    case CREATE_4:
        cerr << "ERROR CODE TABLE: Can't create file" << endl << endl;
        break;

    case SELECT_1:
        cerr << "ERROR CODE TABLE: Wrong size of the thing that you want to get" << endl << endl;
        break;

    case SELECT_2:
        cerr << "ERROR CODE TABLE: Wrong vector size" << endl << endl;
        break;

    case DROP:
        cerr << "ERROR CODE TABLE: Can't drop the table" << endl << endl;
        break;

    case INSERT_1:
        cerr << "ERROR CODE TABLE: Wrong size of the record to insert" << endl << endl;
        break;

    case INSERT_2:
        cerr << "ERROR CODE TABLE: Wrong type of the record to insert" << endl << endl;
        break;

    case INSERT_3:
        cerr << "ERROR CODE TABLE: Size of the record to insert more than size of field" << endl << endl;
        break;

    case UPDATE_1:
        cerr << "ERROR CODE TABLE: Invalid types" << endl << endl;
        break;

    case UPDATE_2:
        cerr << "ERROR CODE TABLE: It won't happen" << endl << endl;
        break;

    case UPDATE_3:
        cerr << "ERROR CODE TABLE: Length of field less than length of string" << endl << endl;
        break;

    case LIKE:
        cerr << "ERROR CODE TABLE: Invalid types, expected TEXT " << endl << endl;
        break;
    }
}
#endif
