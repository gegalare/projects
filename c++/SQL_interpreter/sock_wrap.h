#ifndef __SOCK_WRAP_H__
#define __SOCK_WRAP_H__
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <iostream>
#include <stdexcept>
#include "intr.h"
using namespace std;

const char * address = "my_socket";
class base_socket
{
protected:
	struct sockaddr_un sock_addr;	
	int sock, flag = 0;
    long length;
	string text;
	char tmp;
public:
	base_socket();				//base_socket
	virtual void trade() = 0;		//send + recv
};
/*---------------------------------------------------------------------------*/
class server: public base_socket
{
private:
	int sock_serv;
    FILE *buffer, *errBuffer;
    int fd, errFd;
public:
	server() : base_socket(){};
	~server();				//close for server
	void start();				//bind + listen + accept
	virtual void trade();			//send + recv
	//void analize();
};
/*---------------------------------------------------------------------------*/
class client: public base_socket
{
private:
    int sock_cl;
public:
	client() : base_socket(){};
	~client();				//close for client
	void start();				//connect
	virtual void trade();			//send + recv
};
/*---------------------------------------------------------------------------*/
class Sock_exception
{
private:
	int err_code;
public:
	Sock_exception (int err) : err_code(err){}
	enum Sock_exception_code 
	{
		SOCKET,
		SEND,
		RECEIVE,
		BIND,
		LISTEN,
		ACCEPT,
		CONNECT
	};
	void report();
};
/*---------------------------------------------------------------------------*/
base_socket :: base_socket()
{
	if((sock = socket (AF_UNIX, SOCK_STREAM, 0)) < 0)
	{
		throw Sock_exception(Sock_exception :: SOCKET);
	}
	sock_addr.sun_family = AF_UNIX;
	strcpy(sock_addr.sun_path, address);
}

void server :: start()
{
/*---------------------bind--------------------------------------------------*/
	unlink(address);
	length = sizeof (sock_addr.sun_family) + strlen (sock_addr.sun_path);
	if (bind (sock, (struct sockaddr *)&sock_addr, length) < 0) 
	{
		throw Sock_exception(Sock_exception :: BIND);
	}
/*--------------------listen-------------------------------------------------*/
	if (listen (sock, 1) < 0) 
	{
		throw Sock_exception(Sock_exception :: LISTEN);
	}
	//cout << "Listen" << endl;
	cout << "The server is running" << endl;
/*---------------------accept------------------------------------------------*/
	if ((sock_serv = accept (sock, NULL, NULL)) < 0 ) {
		throw Sock_exception(Sock_exception :: ACCEPT);
	}
	//cout << "Accept" << endl;
	cout << "The client has connected" << endl;
}

void server :: trade()
{
	for(;;)
	{
        buffer = tmpfile();
        fd = fileno(buffer);
        dup2(fd, 1);
        errBuffer = tmpfile();
        errFd = fileno(errBuffer);
        dup2(errFd, 2);
		text.clear();
		if (recv(sock_serv, &length, sizeof(long), 0) < 0)
		{
			throw Sock_exception(Sock_exception :: RECEIVE);
		}
		for(long i = 0; i < length; i++)
		{
			if (recv(sock_serv, &tmp, sizeof(char), 0) < 0)
			{
				throw Sock_exception(Sock_exception :: RECEIVE);
			}
			text.push_back(tmp);
		}
        if (text == "/stop")
		{
			flag = 1;
		}
        analizer :: analize(text);
        fseek(errBuffer, 0, SEEK_END);
        length = ftell(errBuffer);
        if (length > 0 && flag != 1)
        {
            fseek(errBuffer, 0, SEEK_SET);
            if (send(sock_serv, &length, sizeof(long), 0) < 0)
            {
	        	throw Sock_exception(Sock_exception :: RECEIVE);
	        }
            for (long i = 0; i < length; i++)
            {
                fread(&tmp, sizeof(char), 1, errBuffer);
                if (send(sock_serv, &tmp, 1, 0) < 0)
                {
		        	throw Sock_exception(Sock_exception :: RECEIVE);
		        }
            }
        }
        else if (flag != 1)
        {
            fseek(buffer, 0, SEEK_END);
            length = ftell(buffer);
            fseek(buffer, 0, SEEK_SET);
            if (send(sock_serv, &length, sizeof(long), 0) < 0)
            {
	        	throw Sock_exception(Sock_exception :: RECEIVE);
	        }
            for (long i = 0; i < length; i++)
            {
                fread(&tmp, sizeof(char), 1, buffer);
                if (send(sock_serv, &tmp, 1, 0) < 0)
                {
		        	throw Sock_exception(Sock_exception :: RECEIVE);
		        }
            }
        }
        
        fclose(buffer);
        fclose(errBuffer);
        close(fd);
        close(errFd);
        if (flag == 1)
        {
            break;
        }
	}
}

server :: ~server()
{
	close(sock_serv);
}

void client :: start()
{
	length = sizeof (sock_addr.sun_family) + strlen (sock_addr.sun_path);
	if (connect (sock, (struct sockaddr *)&sock_addr, length) < 0)
	{
		throw Sock_exception(Sock_exception :: CONNECT);
	}
	cout << "The connection is being established" << endl;
}

void client :: trade()
{
	for(;;)
	{
        text.clear();
		getline(cin, text);
		length = text.length();

		if (send(sock, &length, sizeof(long), 0) < 0)
		{
			throw Sock_exception(Sock_exception :: SEND);
		}

		for (long i = 0; i < length; i++)
		{
			tmp = text[i];
			if (send(sock, &tmp, 1, 0) < 0)
			{
				throw Sock_exception(Sock_exception :: SEND);
			}
		}

		if (text == "/stop")
		{
			flag = 1;
		}
        
        if (flag != 1)
        {
            if (recv(sock, &length, sizeof(long), 0) < 0)
            {
	        	throw Sock_exception(Sock_exception :: RECEIVE);
	        }

		    for (long i = 0; i < length; i++)
		    {
		        if (recv(sock, &tmp, 1, 0) < 0)
		        {
			        	throw Sock_exception(Sock_exception :: RECEIVE);
			        }
		        cout << tmp;
		    }
        }
        
        if (flag == 1)
        {
            break;
        }
	}
}

client :: ~client()
{
	close(sock);
}

void Sock_exception :: report()
{
	switch(err_code)
	{
		case SOCKET:
			cerr << "ERROR CODE SOCKET: Can't create socket" << endl << endl;
			break;

		case SEND:
			cerr << "ERROR CODE SOCKET: Can't send the message" << endl << endl;
			break;

		case RECEIVE:
			cerr << "ERROR CODE SOCKET: Can't receive the message" << endl << endl;
			break;

		case BIND:
			cerr << "ERROR CODE SOCKET: Can't bind" << endl << endl;
			break;

		case LISTEN:
			cerr << "ERROR CODE SOCKET: Can't listen client" << endl << endl;
			break;

		case ACCEPT:
			cerr << "ERROR CODE SOCKET: Can't accept" << endl << endl;
			break;

		case CONNECT:
			cerr << "ERROR CODE SOCKET: Can't connect to server" << endl << endl;
			break;
	}
}

#endif

