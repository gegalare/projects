#include "sock_wrap.h"
#include "table.h"
#include "intr.h"
int main(void)
{
	try
	{
		client clt;
		clt.start();
		clt.trade();
	} catch (Sock_exception & e) 
	{
		e.report();
	}
	return 0;
}

