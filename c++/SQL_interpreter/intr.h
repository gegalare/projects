#ifndef __INTR_H__
#define __INTR_H__
#include "table.h"
#include <iostream>
#include <cctype>
#include <stdexcept>
#include <string>
#include <stack>

using namespace std;
vector <struct FieldData> in_altr(enum FieldType stateAfterWhr);

class Intr_exception
{
private:
    int err_code;

public:
    Intr_exception(int err) : err_code(err) {}
    enum Intr_exception_code
    {
        CREATE_1, CREATE_2, CREATE_3, CREATE_4, CREATE_5, CREATE_6, CREATE_7, CREATE_8,
        CREATE_9, CREATE_10, CREATE_11, CREATE_12,
        SELECT_1, SELECT_2, SELECT_3, SELECT_4, SELECT_5, SELECT_6, SELECT_7, SELECT_8,
        INSERT_1, INSERT_2, INSERT_3, INSERT_4, INSERT_5, INSERT_6,
        DELETE_1, DELETE_2, DELETE_3, DELETE_4,
        UPDATE_1, UPDATE_2, UPDATE_3, UPDATE_4, UPDATE_5, UPDATE_6, UPDATE_7,
        DROP_1, DROP_2, DROP_3,
        WHERE_1, WHERE_2, WHERE_3, WHERE_4, WHERE_5, WHERE_6, WHERE_7, WHERE_8, WHERE_9,
        WRONG_INPUT_1, WRONG_INPUT_2, WRONG_INPUT_3,
        EXPRESSION_1,
        TERM_1,
        FACTOR_1, FACTOR_2, FACTOR_3, FACTOR_4, FACTOR_5, FACTOR_6, FACTOR_7,
        LOG_EXPR_1,
        LOG_FACTOR_1, LOG_FACTOR_2, LOG_FACTOR_3, LOG_FACTOR_4, LOG_FACTOR_5, 
        LOG_FACTOR_6,
        IN_1, IN_2, IN_3, IN_4
    };
    void report();
};

enum lex_type_t {T_START, T_CREATE, T_SELECT, T_INSERT, T_UPDATE, T_DROP, T_DELETE,
                    T_WHERE, T_LIKE, T_IN, T_FROM, T_TABLE, T_SET, T_INTO, T_NUM, T_STRING,
                    T_LONG, T_TEXT, T_PLUS, T_MINUS, T_ROUND_OPEN, T_ROUND_CLOSE, T_OPEN,
                    T_CLOSE, T_MULT, T_END, T_OK, T_NOT, AT_ND, T_OR, T_IDENT, T_APOSTR,
                    T_COMMA, T_EQL, T_DIV, T_MOD, T_FIELD_TEXT, T_FIELD_LONG, T_ALL,
                    T_NEQL, T_M, T_ME, T_L, T_LE, T_AND, T_STR};

namespace lexer {
    enum lex_type_t  cur_lex_type;
    string cur_lex_text;
    string help_text;
    char c;
    string request;
    int countInit = 0;
    int check = 0;

    void init()
    {
        c = request[countInit];
        //cout << c << " ";
        countInit++;
    }

    void next()
    {	
 
        cur_lex_text.clear();
        enum state_t {START, C, CR, CRE, CREA, CREAT, CREATE, S, SE, SET, SEL, SELE, SELEC, SELECT,
                     I, IN, INS, INSE, INSER, INSERT, INT, INTO, U, UP, UPD, UPDA, UPDAT, UPDATE, D, DR,
                     DRO, DROP, DE, DEL, DELE, DELET, DELETE, T, TA, TE, TEX, TEXT, TAB, TABL, TABLE, N,
                     NO, NOT, O, OR, W, WH, WHE, WHER, WHERE, L, LO, LI, LIK, LON, LIKE, LONG, IDENT,
                     NUM, R_C, R_O, PL, MI, COMMA, AP, OK, END, MULT, EQL, DIV, MOD, F, FR, FRO, FROM, A,
                     AL, ALL, EX_M, NEQL, L_E, M_E, MORE, LESS, AN, AND, STR};
        enum state_t state = START;
        while (state != OK) 
        {
            switch (state) 
            {
                case START:
                    if (isspace(c))
                    {
                        //stay
                    } 
                    else if (c == 'C')
                    {
                        state = C;
                    } 
                    else if (c == 'S') 
                    {
                        state = S;
                    } 
                    else if (c == 'I') 
                    {
                        state = I;
                    } 
                    else if (c == 'U') 
                    {
                        state = U;
                    } 
                    else if (c == 'D') 
                    {
                        state = D;
                    } 
                    else if (c == 'T') 
                    {
                        state = T;
                    } 
                    else if (c == 'N') 
                    {
                        state = N;
                    } 
                    else if (c == 'O') 
                    {
                        state = O;
                    } 
                    else if (c == 'L') 
                    {
                        state = L;
                    } 
                    else if (c == 'A') 
                    {
                        state = A;
                    }
                    else if (c == ',') 
                    {
                        state = COMMA;
                    } 
                    else if (c == '(') 
                    {
                        state = R_O;
                    } 
                    else if (c == ')') 
                    {
                        state = R_C;
                    } 
                    else if (c == '\'') 
                    {
                    	check++;
                        state = STR;
                    } 
                    else if (c == 'W') 
                    {
                        state = W;
                    } 
                    else if (c == '+') 
                    {
                        state = PL;
                    } 
                    else if (c == '-') 
                    {
                        state = MI;
                    } 
                    else if (c == '*')
                    {
                        state = MULT;    
                    } 
                    else if (c == '/')
                    {
                        state = DIV;    
                    } 
                    else if (c == '%')
                    {
                        state = MOD;    
                    } 
                    else if (c == 'F')
                    {
                        state = F;    
                    } 
                    else if (c == '=')
                    {
                        state = EQL;    
                    }
                    else if (c == '>')
                    {
                        state = MORE;    
                    } 
                    else if (c == '<')
                    {
                        state = LESS;    
                    }
                    else if (c == '!')
                    {
                        state = EX_M;    
                    }  
                    else if (isdigit(c))
                    {
                        state = NUM;
                    } 
                    else if (isalpha(c))
                    {
                        state = IDENT;    
                    } 
                    else if (c == '\0') 
                    {
                        cur_lex_type = T_END;
                        state = OK;
                    }
                    else
                    {
                        throw Intr_exception(Intr_exception :: WRONG_INPUT_1);
                    }
                    break;

                case MORE:
                    if (c == '=')
                    {
                        state = M_E;
                    }
                    else if (isspace(c))
                    {
                        cur_lex_type = T_M;
                        state = OK;
                    }
                    break;

                case M_E:
                    state = OK;
                    cur_lex_type = T_ME;
                    break;

		        case STR:
		            if (c == '\'')
			        {
			        	check--;
                        cur_lex_text.push_back('\'');
                        init();
                        state = OK;
                        cur_lex_type = T_STR;
                    }
                    else
                    {
                        //stay
                    }
                    break;


                case LESS:
                    if (c == '=')
                    {
                        state = L_E;
                    }
                    else if (isspace(c))
                    {
                        cur_lex_type = T_L;
                        state = OK;
                    }
                    break;

                case L_E:
                    state = OK;
                    cur_lex_type = T_ME;
                    break;

                case EX_M:
                    if (c == '=')
                    {
                        state = NEQL;
                    }
                    else if (isspace(c))
                    {
                        cur_lex_type = T_EQL;
                        state = OK;
                    }
                    break;
                
                case NEQL:
                    state = OK;
                    cur_lex_type = T_NEQL;
                    break;

                case C:
                    if (c == 'R')
                    {
                        state = CR;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;
                
                case CR:
                    if (c == 'E')
                    {
                        state = CRE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case CRE:
                    if (c == 'A')
                    {
                        state = CREA;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case CREA:
                    if (c == 'T')
                    {
                        state = CREAT;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;
                
                case CREAT:
                    if (c == 'E')
                    {
                        state = CREATE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;
                
                case CREATE:
                    if (isspace(c))
                    {
                        cur_lex_type = T_CREATE;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case S:
                    if (c == 'E')
                    {
                        state = SE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case SE:
                    if (c == 'L')
                    {
                        state = SEL;
                    } 
                    else if (c == 'T')
                    {
                        state = SET;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case SET:
                    if (isspace(c) || c == '\0')
                    {
                        cur_lex_type = T_SET;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case A:
                    if (c == 'L')
                    {
                        state = AL;
                    } 
                    else if (c == 'N')
                    {
                        state = AN;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case AN:
                    if (c == 'D')
                    {
                        state = AND;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case AND:
                    if (isspace(c))
                    {
                        cur_lex_type = T_AND;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case AL:
                    if (c == 'L')
                    {
                        state = ALL;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case ALL:
                    if (c == '\0') 
                    {
                        cur_lex_type = T_ALL;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case SEL:
                    if (c == 'E')
                    {
                        state = SELE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case SELE:
                    if (c == 'C')
                    {
                        state = SELEC;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case SELEC:
                    if (c == 'T')
                    {
                        state = SELECT;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case SELECT:
                    if (isspace(c))
                    {
                        cur_lex_type = T_SELECT;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case I:
                    if (c == 'N')
                    {
                        state = IN;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case IN:
                    if (c == 'T')
                    {
                        state = INT;
                    } 
                    else if (c == 'S')
                    {
                        state = INS;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IN;
                        state = OK;
                    }
                    break;

                case INT:
                    if (c == 'O')
                    {
                        state = INTO;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case INTO:
                    if (isspace(c) || c == '\0')
                    {
                        cur_lex_type = T_INTO;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case INS:
                    if (c == 'E')
                    {
                        state = INSE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case INSE:
                    if (c == 'R')
                    {
                        state = INSER;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case INSER:
                    if (c == 'T')
                    {
                        state = INSERT;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case INSERT:
                    if (isspace(c))
                    {
                        cur_lex_type = T_INSERT;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case U:
                    if (c == 'P')
                    {
                        state = UP;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case UP:
                    if (c == 'D')
                    {
                        state = UPD;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case UPD:
                    if (c == 'A')
                    {
                        state = UPDA;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case UPDA:
                    if (c == 'T')
                    {
                        state = UPDAT;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case UPDAT:
                    if (c == 'E')
                    {
                        state = UPDATE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case UPDATE:
                    if (isspace(c))
                    {
                        cur_lex_type = T_UPDATE;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case D:
                    if (c == 'R')
                    {
                        state = DR;
                    } 
			        else if (c == 'E')
                    {
                        state = DE;
                    }
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case DR:
                    if (c == 'O')
                    {
                        state = DRO;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case DRO:
                    if (c == 'P')
                    {
                        state = DROP;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case DROP:
                    if (isspace(c))
                    {
                        cur_lex_type = T_DROP;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case DE:
                    if (c == 'L')
                    {
                        state = DEL;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case DEL:
                    if (c == 'E')
                    {
                        state = DELE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case DELE:
                    if (c == 'T')
                    {
                        state = DELET;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case DELET:
                    if (c == 'E')
                    {
                        state = DELETE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case DELETE:
                    if (isspace(c))
                    {
                        cur_lex_type = T_DELETE;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case T:
                    if (c == 'A')
                    {
                        state = TA;
                    } 
                    else if (c == 'E')
                    {
                        state = TE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case TA:
                    if (c == 'B')
                    {
                        state = TAB;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case TAB:
                    if (c == 'L')
                    {
                        state = TABL;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case TABL:
                    if (c == 'E')
                    {
                        state = TABLE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case TABLE:
                    if (isspace(c) || c == '\0')
                    {
                        cur_lex_type = T_TABLE;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case TE:
                    if (c == 'X')
                    {
                        state = TEX;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case TEX:
                    if (c == 'T')
                    {
                        state = TEXT;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case TEXT:
                    if (isspace(c) || c == '(')
                    {
                        cur_lex_type = T_TEXT;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;
                
                case N:
                    if (c == 'O')
                    {
                        state = NO;
                   } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case NO:
                    if (c == 'T')
                    {
                        state = NOT;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case NOT:
                    if (isspace(c))
                    {
                        cur_lex_type = T_NOT;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case O:
                    if (c == 'R')
                    {
                        state = OR;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case OR:
                    if (isspace(c))
                    {
                        cur_lex_type = T_OR;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case W:
                    if (c == 'H')
                    {
                        state = WH;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case WH:
                    if (c == 'E')
                    {
                        state = WHE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case WHE:
                    if (c == 'R')
                    {
                        state = WHER;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case WHER:
                    if (c == 'E')
                    {
                        state = WHERE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case WHERE:
                    if (isspace(c) || c == '\0')
                    {
                        cur_lex_type = T_WHERE;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case L:
                    if (c == 'O')
                    {
                        state = LO;
                    } 
                    else if (c == 'I')
                    {
                        state = LI;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case LO:
                    if (c == 'N')
                    {
                        state = LON;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case LON:
                    if (c == 'G')
                    {
                        state = LONG;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case LONG:
                    if (isspace(c) || c == ',' || c == ')' || c == '=')
                    {
                        cur_lex_type = T_LONG;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case LI:
                    if (c == 'K')
                    {
                        state = LIK;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case LIK:
                    if (c == 'E')
                    {
                        state = LIKE;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case LIKE:
                    if (isspace(c) || c == '\0')
                    {
                        cur_lex_type = T_LIKE;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case F:
                    if (c == 'R')
                    {
                        state = FR;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case FR:
                    if (c == 'O')
                    {
                        state = FRO;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case FRO:
                    if (c == 'M')
                    {
                        state = FROM;
                    } 
                    else if (isalpha(c) || isdigit(c))
                    {
                        state = IDENT;
                    }
                    else 
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    }
                    break;

                case FROM:
                    if (isspace(c) || c == '\0')
                    {
                        cur_lex_type = T_FROM;
                        state = OK;
                    }
                    else 
                    {
                        state = IDENT;
                    }
                    break;

                case IDENT:
                    if (isdigit(c) || isalpha(c) || c == '_')
                    {
                        //stay
                    }
                    else if (isspace(c) || c == '\0' || c == ',' || c == ')' || c == '='|| c == '>'|| c == '<' || c == '!')
                    {
                        cur_lex_type = T_IDENT;
                        state = OK;
                    } 
                    else
                    {
                        throw 1;
                    }
                    break;

                case NUM:
                    if (isdigit(c))
                    {
                        //stay
                    }
                    else 
                    {
                        state = OK;
                        cur_lex_type = T_NUM;
                    }
                    break;

                case R_C:
                    cur_lex_type = T_ROUND_CLOSE;
                    state = OK;
                    break;

                case R_O:
                    cur_lex_type = T_ROUND_OPEN;
                    state = OK;
                    break;

                case PL:
                    cur_lex_type = T_PLUS;
                    state = OK;
                    break;

                case MULT:
                    cur_lex_type = T_MULT;
                    state = OK;
                    break;

                case EQL:
                    cur_lex_type = T_EQL;
                    state = OK;
                    break;

                case MI:
                    if (isdigit(c))
                    {
                       state = NUM; 
                    }
                    else
                    {
                        cur_lex_type = T_MINUS;
                        state = OK;
                    }
                    break;

                case AP:
                    cur_lex_type = T_APOSTR;
                    state = OK;
                    break;

		        case COMMA:
                    cur_lex_type = T_COMMA;
                    state = OK;
                    break;

                case DIV:
                    cur_lex_type = T_DIV;
                    state = OK;
                    break;

                case MOD:
                    cur_lex_type = T_MOD;
                    state = OK;
                    break;

                case OK:
                    break;

                default:
                    throw Intr_exception(Intr_exception :: WRONG_INPUT_2);
                    break;
            }

            if (state != OK) 
            {
				if (!isspace(c) && cur_lex_type != T_END) 
                {
                    cur_lex_text.push_back(c);
		            if (check != 0 && cur_lex_type == T_END)
		        	{
		        		throw Intr_exception(Intr_exception :: WRONG_INPUT_2);
		        		break;
		        	}
                }
                lexer :: init();
		    }
        }
    }

}

namespace parser
{

    void init()
    {
        lexer :: init();
        lexer :: next();
    }

    void Start();
    void CRT();
    void DRP();
    void SLCT();
    void UPDT();
    void DLT();
    void INSRT();

    vector <long>  WHR(int count, string fieldName, class Table &table);

    vector <struct FieldData> EXPR(FieldType &tmp_state, class Table &table, int flag = 0);
    vector <struct FieldData> TERM(FieldType &tmp_state, class Table &table, int flag = 0);
    vector <struct FieldData> FACTOR(FieldType &tmp_state, class Table &table, int flag = 0);

    vector <struct FieldData> LOG_TERM(class Table &table);
    vector <struct FieldData> LOG_EXPR(class Table &table);
    vector <struct FieldData> LOG_FACTOR(class Table &table);

    void Start()
    {
        switch(lexer :: cur_lex_type) 
        {
            case T_CREATE:
                lexer :: next();
                CRT();
                break;

            case T_DROP:
                lexer :: next();
                DRP();
                break;

            case T_SELECT:
                lexer :: next();
                SLCT();
                break;

            case T_UPDATE:
                lexer :: next();
                UPDT();
                break;

            case T_DELETE:
                lexer :: next();
                DLT();
                break;

            case T_INSERT:
                lexer :: next();
                INSRT();
                break;

            default:
                throw Intr_exception(Intr_exception :: WRONG_INPUT_3);
        }
    }

/************************ CREATE ************************/
    void CRT()
    {
        string fileName;
        vector <struct FieldInfo> CRTFieldInfo;
        struct  FieldInfo tmpFieldInfo;
        
        if (lexer :: cur_lex_type != T_TABLE)
        {
            throw Intr_exception(Intr_exception :: CREATE_1);
        }

        lexer :: next();
        if (lexer :: cur_lex_type != T_IDENT)
        {
            throw Intr_exception(Intr_exception :: CREATE_2);
        }
        fileName = lexer :: cur_lex_text;

        lexer :: next();
        if (lexer :: cur_lex_type != T_ROUND_OPEN)
        {
            throw Intr_exception(Intr_exception :: CREATE_3);
        }

        lexer :: next();
        int countOfFields = 0;
        while (lexer :: cur_lex_type != T_ROUND_CLOSE)
        {
            if ((countOfFields != 0) && (lexer :: cur_lex_type != T_COMMA))
            {
                throw Intr_exception(Intr_exception :: CREATE_4); 
            } 
            else if (lexer :: cur_lex_type == T_COMMA)
            {
                lexer :: next();
            }

            if (lexer :: cur_lex_type != T_IDENT)
            {
                throw Intr_exception(Intr_exception :: CREATE_5);
            }

            if (lexer :: cur_lex_text.size() > MAX_FIELD_NAME_LEN)
            {
                throw Intr_exception(Intr_exception :: CREATE_6);
            }
            strcpy (tmpFieldInfo.fieldName, lexer :: cur_lex_text.c_str());

            lexer :: next();
            if ((lexer :: cur_lex_type != T_LONG) && (lexer :: cur_lex_type != T_TEXT))
            {
                throw Intr_exception(Intr_exception :: CREATE_7);
            }

            if (lexer :: cur_lex_type == T_LONG)
            {
                tmpFieldInfo.type = LONG;
                tmpFieldInfo.length = sizeof(long);
            } 
            else 
            {
                tmpFieldInfo.type = TEXT;
                
                lexer :: next();
                if (lexer :: cur_lex_type != T_ROUND_OPEN) 
                {
                    throw Intr_exception(Intr_exception :: CREATE_8);
                }

                lexer :: next();
                if (lexer :: cur_lex_type != T_NUM)
                {
                    throw Intr_exception(Intr_exception :: CREATE_9);
                }
                tmpFieldInfo.length = stol(lexer :: cur_lex_text);

                lexer :: next();
                if (lexer :: cur_lex_type != T_ROUND_CLOSE)
                {
                    throw Intr_exception(Intr_exception :: CREATE_10);
                }
            }
            CRTFieldInfo.push_back(tmpFieldInfo);

            lexer :: next();
            countOfFields++;
        }
        if (countOfFields == 0)
        {
            throw Intr_exception(Intr_exception :: CREATE_11);
        }

        lexer :: next();
        if (lexer :: cur_lex_type != T_END)
        {
            throw Intr_exception(Intr_exception :: CREATE_12);
        }

        Table tab(fileName, CRTFieldInfo);
        tab.writeTable();
    }

/************************* DROP *************************/
    void DRP()
    {
        if (lexer :: cur_lex_type != T_TABLE)
        {
           throw Intr_exception(Intr_exception :: DROP_1);
        }

        lexer :: next();
        if (lexer :: cur_lex_type != T_IDENT)
        {
            throw Intr_exception(Intr_exception :: DROP_2);
        }
        string tmp = lexer :: cur_lex_text;

        lexer :: next();
        if (lexer :: cur_lex_type != T_END)
        {
            throw Intr_exception(Intr_exception :: DROP_3);
        }

        drop(tmp);
    }

/************************ SELECT ************************/
    void SLCT()
    {
        string fieldName, fileName;
        vector <struct FieldInfo> OPNFieldInfo;
        vector <string> SLCTFieldName;
        vector <long> SLCTDataOffset;
        int flag = 0;

        if (lexer :: cur_lex_type == T_MULT)
        {
            flag = 1;
            lexer :: next();
        }
        else if (lexer :: cur_lex_type == T_IDENT)
        {
            SLCTFieldName.push_back(lexer :: cur_lex_text);
            lexer :: next();
            while (lexer :: cur_lex_type == T_COMMA)
            {
                if (lexer :: cur_lex_type != T_COMMA)
                {
                    throw Intr_exception(Intr_exception :: SELECT_8);
                }
                lexer :: next();
                if (lexer :: cur_lex_type == T_IDENT)
                {
                    SLCTFieldName.push_back(lexer :: cur_lex_text);
                    lexer :: next();
                }
                else 
                {
                    throw Intr_exception(Intr_exception :: SELECT_1);
                }
            }
            
        }
        else 
        {
            throw Intr_exception(Intr_exception :: SELECT_2);
        }

        if (lexer :: cur_lex_type != T_FROM)
        {
            throw Intr_exception(Intr_exception :: SELECT_3);
        }

        lexer :: next();
        if (lexer :: cur_lex_type != T_IDENT)
        {
            throw Intr_exception(Intr_exception :: SELECT_4);
        }

        fileName = lexer :: cur_lex_text;
        Table tab(fileName, OPNFieldInfo);

        if (flag == 0)
        {
            int check = SLCTFieldName.size();
            for (int i = 0; i < check; i++)
            {
                if ((tab.findField(SLCTFieldName[i]) != TEXT) && (tab.findField(SLCTFieldName[i]) != LONG))
                {
                    throw Intr_exception(Intr_exception :: SELECT_5);
                }
            }
        }

        lexer :: next();
	    if (lexer :: cur_lex_type != T_WHERE)
        {
            throw Intr_exception(Intr_exception :: SELECT_6);
        }

        int tmp_count = lexer :: countInit;
        lexer :: next(); 
        SLCTDataOffset = WHR(tmp_count, lexer :: cur_lex_text, tab);

        if (lexer :: cur_lex_type != T_END)
        {
            throw Intr_exception(Intr_exception :: SELECT_7);
        }
        tab.select(SLCTFieldName, SLCTDataOffset);
    }

/************************ UPDATE ************************/
    void UPDT()
    {
        string fileName;
        vector <struct FieldInfo> OPNFieldInfo;
        vector <long> UPDTDataOffset;
	    vector <struct FieldData> UPDTFieldData;
        struct FieldData UPDTField;
        if (lexer :: cur_lex_type != T_IDENT)
        {
            throw Intr_exception(Intr_exception :: UPDATE_1);
        }

        fileName = lexer :: cur_lex_text;
        Table tab(fileName, OPNFieldInfo);

        lexer :: next();
        if (lexer :: cur_lex_type != T_SET)
        {
            throw Intr_exception(Intr_exception :: UPDATE_2);;
        }

        lexer :: next();
        if (lexer :: cur_lex_type != T_IDENT)
        {
            throw Intr_exception(Intr_exception :: UPDATE_3);
        }

        FieldType tmp_state = tab.findField(lexer :: cur_lex_text);

        if (tmp_state == TEXT)
        {
            UPDTField.type = TEXT;
        }
        else if (tmp_state == LONG)
        {
            UPDTField.type = LONG;
        }
	    else 
	    {
            throw Intr_exception(Intr_exception :: UPDATE_7);
        }

        UPDTField.fieldName = lexer :: cur_lex_text;

        lexer :: next();
        if (lexer :: cur_lex_type != T_EQL)
        {
            throw Intr_exception(Intr_exception :: UPDATE_4);
        }

        lexer :: next();
        UPDTFieldData = EXPR(tmp_state, tab);

        if (lexer :: cur_lex_type != T_WHERE)
        {
            throw Intr_exception(Intr_exception :: UPDATE_5);
        }

        int tmp_count = lexer :: countInit;
        lexer :: next();
        UPDTDataOffset = WHR(tmp_count, lexer :: cur_lex_text, tab);

        if (lexer :: cur_lex_type != T_END)
        {
            throw Intr_exception(Intr_exception :: UPDATE_6);
        }

        tab.update(UPDTField, UPDTFieldData, UPDTDataOffset);
        tab.writeTable();
    }

/************************ DELETE ************************/
    void DLT()
    {
        string fileName, Field;
        vector <struct FieldInfo> OPNFieldInfo;
        vector <long> DLTDataOffset;
        if (lexer :: cur_lex_type != T_FROM)
        {
            throw Intr_exception(Intr_exception :: DELETE_1);
        }
        lexer :: next();

        if (lexer :: cur_lex_type != T_IDENT)
        {
            throw Intr_exception(Intr_exception :: DELETE_2);;
        }

        fileName = lexer :: cur_lex_text;
        Table tab(fileName, OPNFieldInfo);

        lexer :: next();
	    if (lexer :: cur_lex_type != T_WHERE)
        {
            throw Intr_exception(Intr_exception :: DELETE_3);
        }

        int tmp_count = lexer :: countInit;
        lexer :: next();
        DLTDataOffset = WHR(tmp_count, lexer :: cur_lex_text, tab);
        
        if (lexer :: cur_lex_type != T_END)
        {
            throw Intr_exception(Intr_exception :: DELETE_4);
        }

        tab.deleteField(DLTDataOffset);
        tab.writeTable();
    }

/************************ INSERT ************************/  
    void INSRT()
    {
        string fileName;
        vector <struct FieldData> INSRTFieldData;
        vector <struct FieldInfo> OPNFieldInfo;
        struct FieldInfo TMPFieldInfo;
        struct FieldData TMPFieldData;

        if (lexer :: cur_lex_type != T_INTO)
        {
            throw Intr_exception(Intr_exception :: INSERT_1);
        }

        lexer :: next();
        if (lexer :: cur_lex_type != T_IDENT)
        {
           throw Intr_exception(Intr_exception :: INSERT_2);
        }
        fileName = lexer :: cur_lex_text;

        lexer :: next();
        if (lexer :: cur_lex_type != T_ROUND_OPEN)
        {
            throw Intr_exception(Intr_exception :: INSERT_3);
        }

        lexer :: next();

        int countOfFields = 0;
        Table tab(fileName, OPNFieldInfo);
        while (lexer :: cur_lex_type != T_ROUND_CLOSE)
        {
            string tmp;
            if ((countOfFields != 0) && (lexer :: cur_lex_type != T_COMMA))
            {
                throw Intr_exception(Intr_exception :: INSERT_4);
            } 
            else if (lexer :: cur_lex_type == T_COMMA)
            {
                lexer :: next();
            }
            countOfFields++;

            if  (lexer :: cur_lex_type == T_NUM)
            {
                TMPFieldData.type = LONG;
                TMPFieldData.number = stol(lexer :: cur_lex_text);
            }
            else if (lexer :: cur_lex_type == T_STR)
            {
                lexer :: cur_lex_text.erase(0, 1);
                lexer :: cur_lex_text.erase(lexer :: cur_lex_text.length() - 1, 1);
                TMPFieldData.type = TEXT;
                TMPFieldData.fieldName = lexer :: cur_lex_text;
            }
            else 
            {
                throw Intr_exception(Intr_exception :: INSERT_5);
            }

            INSRTFieldData.push_back(TMPFieldData);
            lexer :: next();
        }
	
	    lexer :: next();
        if (lexer :: cur_lex_type != T_END)
        {
            throw Intr_exception(Intr_exception :: INSERT_6);
        }

        tab.insert(INSRTFieldData);
        tab.writeTable();
    }

/************************* EXPR *************************/
    vector <struct FieldData> EXPR(FieldType &tmp_state, class Table &table, int flag)
    {

        vector <struct FieldData> vTmp1;
        vTmp1 = TERM(tmp_state, table, flag);
        if (vTmp1[0].type == ERR)
        {
            vTmp1.clear();
            struct FieldData convert;
            convert.type = ERR;
            vTmp1.push_back(convert);
            return vTmp1;
        }
        while (lexer :: cur_lex_type == T_PLUS || lexer :: cur_lex_type  == T_MINUS) 
        {
            if (tmp_state == TEXT || tmp_state == STR)
            {
                throw Intr_exception(Intr_exception :: EXPRESSION_1);
            }

            enum lex_type_t tmp = lexer :: cur_lex_type;
            lexer :: next();
            vector <struct FieldData> vTmp2;
            vTmp2 = TERM(tmp_state, table, flag);
            int vTmp2Size = vTmp2.size();

            for (int i = 0; i < vTmp2Size; i++)
            {
                vTmp1.push_back(vTmp2[i]);
            }

            if (tmp == T_PLUS)
            {
                struct FieldData convert;
                convert.fieldName = "+";
                convert.type = OP;
                vTmp1.push_back(convert);
            }
            else if (tmp == T_MINUS)
            {
                struct FieldData convert;
                convert.fieldName = "-";
                convert.type = OP;
                vTmp1.push_back(convert);
            }
            
        }
        if ((lexer :: cur_lex_type == T_EQL || lexer :: cur_lex_type == T_NEQL
                || lexer :: cur_lex_type == T_M || lexer :: cur_lex_type == T_ME
                || lexer :: cur_lex_type == T_L || lexer :: cur_lex_type == T_LE) 
                && (flag == 0))
        {

            vTmp1.clear();
            struct FieldData convert;
            convert.type = ERR;
            vTmp1.push_back(convert);
            return vTmp1;
        }
        return vTmp1;
    }

/************************* TERM *************************/
    vector <struct FieldData> TERM(FieldType &tmp_state, class Table &table, int flag)
    {
        vector <struct FieldData> vTmp3; 
        vTmp3 = FACTOR(tmp_state, table, flag);
        if (vTmp3[0].type == ERR)
        {
            vTmp3.clear();
            struct FieldData convert;
            convert.type = ERR;
            vTmp3.push_back(convert);
            return vTmp3;
        }
        while (lexer :: cur_lex_type == T_MULT || lexer :: cur_lex_type == T_DIV ||
                                                  lexer :: cur_lex_type == T_MOD) 
        {
            if (tmp_state == TEXT || tmp_state == STR)
            {
                throw Intr_exception(Intr_exception :: TERM_1);
            }

            enum lex_type_t tmp = lexer :: cur_lex_type;
            lexer :: next();
            vector <struct FieldData> vTmp4;
            vTmp4 = FACTOR(tmp_state, table, flag);
            int vTmp4Size = vTmp4.size();

            for (int i = 0; i < vTmp4Size; i++)
            {
                vTmp3.push_back(vTmp4[i]);
            }

            if (tmp == T_MULT)
            {
                struct FieldData convert;
                convert.fieldName = "*";
                convert.type = OP;
                vTmp3.push_back(convert);
            }
            else if (tmp == T_DIV)
            {
                struct FieldData convert;
                convert.fieldName = "/";
                convert.type = OP;
                vTmp3.push_back(convert);
            }
            else if (tmp == T_MOD)
            {
                struct FieldData convert;
                convert.fieldName = "%";
                convert.type = OP;
                vTmp3.push_back(convert);
            }
        }
        return vTmp3;
    }

/************************ FACTOR ************************/
    vector <struct FieldData> FACTOR(FieldType &tmp_state, class Table &table, int flag)
    {
        vector <struct FieldData> vTmp5;
        if (lexer :: cur_lex_type == T_ROUND_OPEN)
        {
            vector <struct FieldData> vTmp6;
            lexer :: next();
            if (lexer :: cur_lex_type == T_STR)
            {
                tmp_state = STR;
            }
            else if (lexer :: cur_lex_type == T_NUM)
            {
                tmp_state = LONG;
            }
            else
            {
                tmp_state = table.findField(lexer :: cur_lex_text);
            }
            vTmp6 = EXPR(tmp_state, table, flag);

            int vTmp6Size = vTmp6.size();
            for (int i = 0; i < vTmp6Size; i++)
            {
                vTmp5.push_back(vTmp6[i]);
            }
            if ((lexer :: cur_lex_type == T_EQL || lexer :: cur_lex_type == T_NEQL
                || lexer :: cur_lex_type == T_M || lexer :: cur_lex_type == T_ME
                || lexer :: cur_lex_type == T_L || lexer :: cur_lex_type == T_LE
                || lexer :: cur_lex_type == T_NOT) && (flag == 0))
            {
                vTmp5.clear();
                struct FieldData convert;
                convert.type = ERR;
                vTmp5.push_back(convert);
                return vTmp5;
            }
            else if ((lexer :: cur_lex_type != T_ROUND_CLOSE) && (flag != 2))
            {
                throw Intr_exception(Intr_exception :: FACTOR_1);
            }
            else if ((lexer :: cur_lex_type != T_ROUND_CLOSE) && (flag == 2))
            {
                vTmp5.clear();
                struct FieldData convert;
                convert.type = ERR;
                vTmp5.push_back(convert);
                return vTmp5;
            }

            lexer :: next();
            return vTmp5;
        }
        else if (lexer :: cur_lex_type == T_IDENT) 
        {
            struct FieldData convert;
            if (lexer :: cur_lex_type == T_STR)
            {
                tmp_state = STR;
            }
            else if (lexer :: cur_lex_type == T_NUM)
            {
                tmp_state = LONG;
            }
            else
            {
                tmp_state = table.findField(lexer :: cur_lex_text);
            }

            if (table.findField(lexer :: cur_lex_text) == TEXT)
            {
                if (tmp_state == LONG || tmp_state == NUM)
                {
                    throw Intr_exception(Intr_exception :: FACTOR_2);
                }
                convert.type = TEXT;
            }
            else if (table.findField(lexer :: cur_lex_text) == LONG)
            {
                if (tmp_state == TEXT || tmp_state == STR)
                {
                    throw Intr_exception(Intr_exception :: FACTOR_3);
                }
                convert.type = LONG;
            }
            else
            {
                throw Intr_exception(Intr_exception :: FACTOR_4);
            }
            
            if (lexer :: cur_lex_type == T_STR)
            {
                tmp_state = STR;
            }
            else if (lexer :: cur_lex_type == T_NUM)
            {
                tmp_state = LONG;
            }
            else
            {
                tmp_state = table.findField(lexer :: cur_lex_text);
            }

            convert.fieldName = lexer :: cur_lex_text;        
            vTmp5.push_back(convert);
            lexer :: next();
            return vTmp5;
        } 
        else if  (lexer :: cur_lex_type == T_NUM)
        {
            if (tmp_state == TEXT)
            {
                throw Intr_exception(Intr_exception :: FACTOR_5);
            }
            tmp_state = NUM;

            struct FieldData convert;
            convert.number = stol(lexer :: cur_lex_text);
            convert.type = NUM;
            vTmp5.push_back(convert);
            lexer :: next();
            return vTmp5;
        }
        else if  (lexer :: cur_lex_type == T_STR)
        {
            if (tmp_state == LONG)
            {
                throw Intr_exception(Intr_exception :: FACTOR_6);
            }
            tmp_state = STR;

            struct FieldData convert;
            lexer :: cur_lex_text.erase(0, 1);
	        lexer :: cur_lex_text.erase(lexer :: cur_lex_text.length() - 1, 1);
            convert.fieldName = lexer :: cur_lex_text;
            convert.type = STR;
            vTmp5.push_back(convert);
            lexer :: next();
            return vTmp5;
        }
        else if (lexer :: cur_lex_type == T_NOT)
        {
            vTmp5.clear();
            struct FieldData convert;
            convert.type = ERR;
            vTmp5.push_back(convert);
            return vTmp5;
        }
        else if (flag == 2)
        {
            vTmp5.clear();
            struct FieldData convert;
            convert.type = ERR;
            vTmp5.push_back(convert);
            return vTmp5;
        }
        else 
        {
            throw Intr_exception(Intr_exception :: FACTOR_7);
        }
        
    }

/************************* LOG EXPR *************************/
    vector <struct FieldData> LOG_EXPR(class Table &table)
    {
        vector <struct FieldData> vTmp1;
        vTmp1 = LOG_TERM(table);
        while (lexer :: cur_lex_type == T_OR) 
        {
            enum lex_type_t tmp = lexer :: cur_lex_type;
            lexer :: next();

            vector <struct FieldData> vTmp2;
            vTmp2 = LOG_TERM(table);
            int vTmp2Size = vTmp2.size();

            for (int i = 0; i < vTmp2Size; i++)
            {
                vTmp1.push_back(vTmp2[i]);
            }

            if (tmp == T_OR)
            {
                struct FieldData convert;
                convert.fieldName = "OR";
                convert.type = OR;
                vTmp1.push_back(convert);
            }

            if (lexer :: cur_lex_type != T_ROUND_CLOSE)
            {
                throw Intr_exception(Intr_exception :: LOG_EXPR_1);
            }
        }
        return vTmp1;
    }

/************************* LOG MULT *************************/
    vector <struct FieldData> LOG_TERM(class Table &table)
    {
        vector <struct FieldData> vTmp3; 
        vTmp3 = LOG_FACTOR(table);
        while (lexer :: cur_lex_type == T_AND)
        {
            enum lex_type_t tmp = lexer :: cur_lex_type;
            lexer :: next();
    
            vector <struct FieldData> vTmp4;
            vTmp4 = LOG_FACTOR(table);
            int vTmp4Size = vTmp4.size();

            for (int i = 0; i < vTmp4Size; i++)
            {
                vTmp3.push_back(vTmp4[i]);
            }

            if (tmp == T_AND)
            {
                struct FieldData convert;
                convert.fieldName = "AND";
                convert.type = AND;
                vTmp3.push_back(convert);
            }
        }
        return vTmp3;
    }

/************************ LOG_FACTOR ************************/
    vector <struct FieldData> LOG_FACTOR(class Table &table)
    {
        vector <struct FieldData> vTmp5;
        if (lexer :: cur_lex_type == T_ROUND_OPEN)
        {
            int logFlag = 0, flag1 = 2, flag = 0;
            vector <struct FieldData> vTmp6;
            int tmp_pos = lexer :: countInit;
            lexer :: next();

            FieldType tmp_state = ZERO;
            vTmp6 = EXPR(tmp_state, table, flag1);

            if (vTmp6[0].type != ERR)
            {
                logFlag = 1;

            }
            lexer :: countInit = tmp_pos - 1;
            lexer :: init();
            lexer :: next();
            if ((lexer :: cur_lex_type == T_IDENT ||
                lexer :: cur_lex_type == T_STR ||
                lexer :: cur_lex_type == T_NUM) || (logFlag == 1))
            {
                flag = 1;
                tmp_state = ZERO;
                FieldType another_tmp_state = ZERO;
                vTmp6 = EXPR(tmp_state, table, flag);
                int vTmp6Size = vTmp6.size();

                for (int i = 0; i < vTmp6Size; i++)
                {
                    vTmp5.push_back(vTmp6[i]);
                }

                enum lex_type_t tmp = lexer :: cur_lex_type;
                lexer :: next();
                vTmp6 = EXPR(another_tmp_state, table, flag);
                if (((tmp_state == STR || tmp_state == TEXT) &&
                   (another_tmp_state == LONG || another_tmp_state == NUM)) || 
                   ((another_tmp_state == STR || another_tmp_state == TEXT) &&
                   (tmp_state == LONG || tmp_state == NUM)))
                {
                    throw Intr_exception(Intr_exception :: LOG_FACTOR_1);
                }

                vTmp6Size = vTmp6.size();

                for (int i = 0; i < vTmp6Size; i++)
                {
                    vTmp5.push_back(vTmp6[i]);
                }

                if (tmp == T_EQL)
                {
                
                    struct FieldData convert;
                    convert.fieldName = "=";
                    convert.type = OP_CMP;
                    vTmp5.push_back(convert);
                
                }
                else if (tmp == T_NEQL)
                {
                    struct FieldData convert;
                    convert.fieldName = "!=";
                    convert.type = OP_CMP;
                    vTmp5.push_back(convert);
                }
                else if (tmp == T_M)
                {
                    struct FieldData convert;
                    convert.fieldName = ">";
                    convert.type = OP_CMP;
                    vTmp5.push_back(convert);
                }
                else if (tmp == T_ME)
                {
                    struct FieldData convert;
                    convert.fieldName = "<=";
                    convert.type = OP_CMP;
                    vTmp5.push_back(convert);
                }
                else if (tmp == T_L)
                {
                    struct FieldData convert;
                    convert.fieldName = "<";
                    convert.type = OP_CMP;
                    vTmp5.push_back(convert);
                }
                else if (tmp == T_LE)
                {
                    struct FieldData convert;
                    convert.fieldName = "=>";
                    convert.type = OP_CMP;
                    vTmp5.push_back(convert);
                }
                else 
                {
                    throw Intr_exception(Intr_exception :: LOG_FACTOR_2);
                }

                if (lexer :: cur_lex_type != T_ROUND_CLOSE)
                {
                    throw Intr_exception(Intr_exception :: LOG_FACTOR_3);
                }
                lexer :: next();
                return vTmp5;
            
            }
            else if (lexer :: cur_lex_type == T_ROUND_OPEN)         
            {
                vTmp6 = LOG_EXPR(table);
            }
            else if (lexer :: cur_lex_type == T_NOT)
            {
                vTmp6 = LOG_EXPR(table);
            }
            else
            {
                throw Intr_exception(Intr_exception :: LOG_FACTOR_4);
            }
            

            int vTmp6Size = vTmp6.size();

            for (int i = 0; i < vTmp6Size; i++)
            {
                vTmp5.push_back(vTmp6[i]);
            }

            if (lexer :: cur_lex_type != T_ROUND_CLOSE) 
            {
                throw Intr_exception(Intr_exception :: LOG_FACTOR_5);
            }

            lexer :: next();
            return vTmp5;
        }
        else if (lexer :: cur_lex_type == T_NOT)
        {
            struct FieldData convert;
            convert.fieldName = "NOT";
            convert.type = NOT;
            lexer :: next();
            vTmp5 = LOG_FACTOR(table);
            vTmp5.push_back(convert);
            return vTmp5;
        }
        else
        {
            throw Intr_exception(Intr_exception :: LOG_FACTOR_6);
        }
        
    }


/************************ WHERE *************************/
    vector <long>  WHR(int count, string fieldName, class Table &table)
    {   
        int flag = 0;
        vector <long> recordOffset;
        if (lexer :: cur_lex_type == T_ALL)
        {
            recordOffset = table.all(fieldName);
            lexer :: next();
        }
        else if (lexer :: cur_lex_type == T_NOT)
        {
            vector <struct FieldData> tmpFieldData;
            tmpFieldData = LOG_EXPR(table);
            recordOffset = table.boolean(tmpFieldData);
        }
        else if (lexer :: cur_lex_type == T_ROUND_OPEN || 
                 lexer :: cur_lex_type == T_IDENT)
        {
            enum FieldType stateAfterWhr;
            if (lexer :: cur_lex_type == T_IDENT)
            {
                stateAfterWhr = table.findField(lexer :: cur_lex_text);
            }

            vector <struct FieldData> tmpFieldData;
            FieldType tmp_state = ZERO;
            tmpFieldData = EXPR(tmp_state, table);
            if (tmpFieldData[0].type == ERR)
            {
                lexer :: countInit = count - 1;
                lexer :: init();
                lexer :: next();
                tmpFieldData = LOG_EXPR(table);
                recordOffset = table.boolean(tmpFieldData);
            }
            else
            {
                int size = tmpFieldData.size();
                if ((size == 1) && (tmpFieldData[0].type == TEXT))
                {
                    if (lexer :: cur_lex_type == T_LIKE)
                    {
                        lexer :: next();
                        if (lexer :: cur_lex_type == T_STR)
                        {
                            lexer :: cur_lex_text.erase(0, 1);
                	        lexer :: cur_lex_text.erase(lexer :: cur_lex_text.length() - 1, 1);
                            MakeStrGreatAgain(lexer :: cur_lex_text);
                            recordOffset = table.like(fieldName, lexer :: cur_lex_text, flag);
                            lexer :: next();
                        }
                        else
                        {
                            throw Intr_exception(Intr_exception :: WHERE_1);
                        }
                    }
                    else if (lexer :: cur_lex_type == T_IN)
                    {
                        vector <struct FieldData> vIn;
                        enum FieldType state;
                        vector <long> toChangeLong;
                        vector <string> toChangeStr;
                        lexer :: next();
                        vIn = in_altr(stateAfterWhr);
                        if (vIn.empty())
                        {
                            throw Intr_exception(Intr_exception :: WHERE_2);
                        }
                        state = table.poliz_analize(toChangeLong, toChangeStr, tmpFieldData);
                        recordOffset = table.in(toChangeLong, toChangeStr, vIn, state, flag);
                        lexer :: next();
                    }
                    else if (lexer :: cur_lex_type == T_NOT)
                    {
                        flag = 1;
                        lexer :: next();
                        if (lexer :: cur_lex_type == T_LIKE)
                        {
                            lexer :: next();
                            if (lexer :: cur_lex_type == T_STR)
                            {
                                lexer :: cur_lex_text.erase(0, 1);
                    	        lexer :: cur_lex_text.erase(lexer :: cur_lex_text.length() - 1, 1);
                                MakeStrGreatAgain(lexer :: cur_lex_text);
                                recordOffset = table.like(fieldName, lexer :: cur_lex_text, flag);
                                lexer :: next();
                            }
                            else
                            {
                                throw Intr_exception(Intr_exception :: WHERE_3);
                            }
                            
                        }
                        else if (lexer :: cur_lex_type == T_IN)
                        {
                            vector <struct FieldData> vIn;
                            enum FieldType state;
                            vector <long> toChangeLong;
                            vector <string> toChangeStr;
        
                            lexer :: next();
                            vIn = in_altr(stateAfterWhr);
                            if (vIn.empty())
                            {
                                throw Intr_exception(Intr_exception :: WHERE_4);
                            }
                            state = table.poliz_analize(toChangeLong, toChangeStr, tmpFieldData);
                            recordOffset = table.in(toChangeLong, toChangeStr, vIn, state, flag);
                            lexer :: next();
                        }
                        else
                        {
                            throw Intr_exception(Intr_exception :: WHERE_5);
                        }
                    } 
                    else 
                    {
                         throw Intr_exception(Intr_exception :: WHERE_9);
                    }
                    
                }
                else
                {
                    if (lexer :: cur_lex_type == T_IN)
                    {
                        vector <struct FieldData> vIn;
                        enum FieldType state;
                        vector <long> toChangeLong;
                        vector <string> toChangeStr;
                        lexer :: next();
                        vIn = in_altr(stateAfterWhr);
                        if (vIn.empty())
                        {
                            throw Intr_exception(Intr_exception :: WHERE_6);
                        }
                        state = table.poliz_analize(toChangeLong, toChangeStr, tmpFieldData);
                        recordOffset = table.in(toChangeLong, toChangeStr, vIn, state, flag);
                        lexer :: next();
                    }
                    else if (lexer :: cur_lex_type == T_NOT)
                    {
                        lexer :: next();
                        flag = 1;
                        if (lexer :: cur_lex_type == T_IN)
                        {
                            vector <struct FieldData> vIn;
                            enum FieldType state;
                            vector <long> toChangeLong;
                            vector <string> toChangeStr;

                            lexer :: next();
                            vIn = in_altr(stateAfterWhr);
                            if (vIn.empty())
                            {
                                throw Intr_exception(Intr_exception :: WHERE_7);
                            }
                            state = table.poliz_analize(toChangeLong, toChangeStr, tmpFieldData);
                            recordOffset = table.in(toChangeLong, toChangeStr, vIn, state, flag);
                            lexer :: next();
                        }
                    }
                    
                }
            }
        }
        else
        {
            throw Intr_exception(Intr_exception :: WHERE_8);
        }
        return recordOffset;
    }
}

namespace analizer
{
    void analize(string text)
    {
        try
        {
            lexer :: request.clear();
            lexer :: countInit = 0;
            lexer :: request = text;
            lexer :: request + '\0';
            parser :: init();
            parser :: Start();
        } catch (Intr_exception & e)
        {
            e.report();
        }
        catch (Table_exception & e)
        {
            e.report();
        }

    }
}

void MakeStrGreatAgain(string &str)
{
    int tmp = str.size();
    for(int i = 0; i < tmp; i++)
    {
        if (str[i] == '%')
        {
            str[i] = '*';
            str.insert(str.begin() + i, '.');
            i++;
        }
        else if (str[i] == '_')
        {
            str[i] = '.';
        }
        else if ((str[i] == '.') || (str[i] == '*'))
        {
            str.insert(str.begin() + i, '\\');
        }
    }
}

vector <struct FieldData> in_altr(enum FieldType stateAfterWhr)
{
    enum FieldType state = ZERO;
    vector <struct FieldData> vIn;
    if (lexer :: cur_lex_type == T_ROUND_OPEN)
    {
        lexer :: next();
        while (lexer :: cur_lex_type != T_ROUND_CLOSE)
        {
            if ((lexer :: cur_lex_type == T_STR) && (state == ZERO || state == STR) &&
                 stateAfterWhr == TEXT)
            {
                state = STR;
                struct FieldData INFieldData;
                lexer :: cur_lex_text.erase(0, 1);
                lexer :: cur_lex_text.erase(lexer :: cur_lex_text.length() - 1, 1);
                INFieldData.type = STR;
                INFieldData.fieldName = lexer :: cur_lex_text;
                vIn.push_back(INFieldData);
            }
            else if ((lexer :: cur_lex_type == T_NUM) && (state == ZERO || state == NUM) &&
                 stateAfterWhr == LONG)
            {
                state = NUM;
                struct FieldData INFieldData;
                INFieldData.type = NUM;
                INFieldData.number = stol(lexer :: cur_lex_text);
                vIn.push_back(INFieldData);
            }
            else
            {
                throw Intr_exception(Intr_exception :: IN_1);
            }

            lexer :: next();
            if (lexer :: cur_lex_type == T_COMMA) 
            {
                lexer :: next();
                if ((lexer :: cur_lex_type == T_STR && state == NUM && stateAfterWhr == LONG)
                    || (lexer :: cur_lex_type == T_NUM && state == STR && stateAfterWhr == TEXT))
                {
                    throw Intr_exception(Intr_exception :: IN_2);
                }
            }
            else if (lexer :: cur_lex_type == T_ROUND_CLOSE)
            {
                //do nothing 
            }
            else 
            {
                throw Intr_exception(Intr_exception :: IN_3); 
            }
        }
    }
    else
    {
        throw Intr_exception(Intr_exception :: IN_4);
    }
    return vIn;
}

void Intr_exception :: report()
{
    switch (err_code)
    {
    case CREATE_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *TABLE* #1" << endl << endl;
        break;

    case CREATE_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the table* #1" << endl << endl;
        break;

    case CREATE_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *(* #1" << endl << endl;
        break;

    case CREATE_4:
        cerr << "ERROR CODE INTERPRETATOR: Expected *)* or *,* #1" << endl << endl;
        break;

    case CREATE_5:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the field* #1" << endl << endl;
        break;

    case CREATE_6:
        cerr << "ERROR CODE INTERPRETATOR: Size of the field name more than 20" << endl << endl;
        break;

    case CREATE_7:
        cerr << "ERROR CODE INTERPRETATOR: Wrong type of the field" << endl << "Expected *TEXT* or *LONG*" << endl << endl;
        break;

    case CREATE_8:
        cerr << "ERROR CODE INTERPRETATOR: Expected *(* #2" << endl << endl;
        break;

    case CREATE_9:
        cerr << "ERROR CODE INTERPRETATOR: Expected number" << endl << endl;
        break;

    case CREATE_10:
        cerr << "ERROR CODE INTERPRETATOR: Expected *)* #1" << endl << endl;
        break;

    case CREATE_11:
        cerr << "ERROR CODE INTERPRETATOR: Input is empty" << endl << endl;
        break;
    
    case CREATE_12:
        cerr << "ERROR CODE INTERPRETATOR: Expected end of input #1" << endl << endl;
        break;

    case SELECT_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the field* #2" << endl << endl;
        break;

    case SELECT_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected * or *Name of the field*" << endl << endl;
        break;

    case SELECT_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *FROM* #1" << endl << endl;
        break;

    case SELECT_4:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the table* #2" << endl << endl;
        break;

    case SELECT_5:
        cerr << "ERROR CODE INTERPRETATOR: Wrong type of the field" << endl << "Expected *TEXT* or *LONG*" << endl << endl;
        break;

    case SELECT_6:
        cerr << "ERROR CODE INTERPRETATOR: Expected *WHERE* #1" << endl << endl;
        break;

    case SELECT_7:
        cerr << "ERROR CODE INTERPRETATOR: Expected end of input #2" << endl << endl;
        break;

    case SELECT_8:
        cerr << "ERROR CODE INTERPRETATOR: SELECT 8" << endl << endl;
        break;

    case INSERT_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *INTO*" << endl << endl;
        break;

    case INSERT_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the table* #4" << endl << endl;
        break;

    case INSERT_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *(* #3" << endl << endl;
        break;

    case INSERT_4:
        cerr << "ERROR CODE INTERPRETATOR: Expected *)* or *,* #2" << endl << endl;
        break;

    case INSERT_5:
        cerr << "ERROR CODE INTERPRETATOR: Expected number or string #1" << endl << endl;
        break;

    case INSERT_6:
        cerr << "ERROR CODE INTERPRETATOR: Expected end of input #3" << endl << endl;
        break;

    case DELETE_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *FROM* #2" << endl << endl;
        break;

    case DELETE_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the table* #5" << endl << endl;
        break;

    case DELETE_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *WHERE* #2" << endl << endl;
        break;

    case DELETE_4:
        cerr << "ERROR CODE INTERPRETATOR: Expected end of input #4" << endl << endl;
        break;

    case UPDATE_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the table* #6" << endl << endl;
        break;

    case UPDATE_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected *SET*" << endl << endl;
        break;

    case UPDATE_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the field* #3" << endl << endl;
        break;

    case UPDATE_4:
        cerr << "ERROR CODE INTERPRETATOR: Expected *=*" << endl << endl;
        break;

    case UPDATE_5:
        cerr << "ERROR CODE INTERPRETATOR: Expected *WHERE* #3" << endl << endl;
        break;

    case UPDATE_6:
        cerr << "ERROR CODE INTERPRETATOR: Expected end of input #5" << endl << endl;
        break;

    case UPDATE_7:
        cerr << "ERROR CODE INTERPRETATOR: Field with that name does't exist" << endl << endl;
        break;

    case DROP_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *TABLE* #2" << endl << endl;
        break;

    case DROP_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected *Name of the table* #7" << endl << endl;
        break;

    case DROP_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected end of input #6" << endl << endl;
        break;

    case WHERE_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected string #1" << endl << endl;
        break;

    case WHERE_2:
        cerr << "ERROR CODE INTERPRETATOR: IN is empty #1" << endl << endl;
        break;

    case WHERE_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected string #2" << endl << endl;
        break;

    case WHERE_4:
        cerr << "ERROR CODE INTERPRETATOR: IN is empty #2" << endl << endl;
        break;

    case WHERE_5:
        cerr << "ERROR CODE INTERPRETATOR: Expected *IN*" << endl << endl;
        break;

    case WHERE_6:
        cerr << "ERROR CODE INTERPRETATOR: IN is empty #3" << endl << endl;
        break;

    case WHERE_7:
        cerr << "ERROR CODE INTERPRETATOR: IN is empty #4" << endl << endl;
        break;

    case WHERE_8:
        cerr << "ERROR CODE INTERPRETATOR: After WHERE is empty" << endl << endl;
        break;

    case WHERE_9:
        cerr << "ERROR CODE INTERPRETATOR: Expected Where-alternative" << endl << endl;
        break;

    case WRONG_INPUT_1:
        cerr << "ERROR CODE INTERPRETATOR: Wrong input. How did you do that?\
		Check the first symbol" << endl << endl;
        break;

    case WRONG_INPUT_2:
        cerr << "ERROR CODE INTERPRETATOR: Wrong input. That word doesn't exist" << endl << endl;
        break;

    case WRONG_INPUT_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *CREATE* or *INSERT* or" << endl
             << "*DROP* or *DELETE* or *SELECT* or *UPDATE" << endl << endl;
        break;
    
    case EXPRESSION_1:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types* #1" << endl << endl;
        break;

    case TERM_1:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types* #2" << endl << endl;
        break;

    case FACTOR_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *)* #2" << endl << endl;
        break;
    
    case FACTOR_2:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types* #3" << endl << endl;
        break;

    case FACTOR_3:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types* #4" << endl << endl;
        break;

    case FACTOR_4:
        cerr << "ERROR CODE INTERPRETATOR: *Invalid type*" << endl << endl;
        break;

    case FACTOR_5:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types* #5" << endl << endl;
        break;

    case FACTOR_6:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types* #6" << endl << endl;
        break;

    case FACTOR_7:
        cerr << "ERROR CODE INTERPRETATOR: Wrong input, try again #3 " << endl << endl;
        break;

    case LOG_EXPR_1:
        cerr << "ERROR CODE INTERPRETATOR: Expected *)* #3" << endl << endl;
        break;

    case LOG_FACTOR_1:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types of expressions*" << endl << endl;
        break;

    case LOG_FACTOR_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected *>* or *>=* or *<* or *<=*\
                or *=* or *!=*" << endl << endl;
        break;

    case LOG_FACTOR_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *)* #4" << endl << endl;
        break;

    case LOG_FACTOR_4:
        cerr << "ERROR CODE INTERPRETATOR: Expected *NOT* or *(*" << endl << endl;
        break;

    case LOG_FACTOR_5:
        cerr << "ERROR CODE INTERPRETATOR: Expected *)* #5" << endl << endl;
        break;

    case LOG_FACTOR_6:
        cerr << "ERROR CODE INTERPRETATOR: Wrong input, try again #4 " << endl << endl;
        break;

    case IN_1:
        cerr << "ERROR CODE INTERPRETATOR: *Incompatible types* #7" << endl << endl;
        break;

    case IN_2:
        cerr << "ERROR CODE INTERPRETATOR: Expected number or string #2" << endl << endl;
        break;

    case IN_3:
        cerr << "ERROR CODE INTERPRETATOR: Expected *,* or *)*" << endl << endl;
        break;

    case IN_4:
        cerr << "ERROR CODE INTERPRETATOR: Expected *(* #4" << endl << endl;
        break;
    }
}
#endif
