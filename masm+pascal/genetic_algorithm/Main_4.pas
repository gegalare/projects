﻿program Main;
uses dos, Types_4, Reading_4, Genetic_algorithm_4;

var
  mutation_type, crossbreeding_type, variability, preserved_low_positions, 
  preserved_high_positions, population_volume, max_iters, max_valueless_iters, 
  crossing_volume, mode, onscreen, {changing:} i, k, iterations, valueless_iters: integer;
  f, new_file: text;
  Res, L: mas;
  enough_function_value, quality_epsilon, {changing:} population_quality, previous_value: real;
  
  Hour1, Minute1, Second1, Sec1001: word;
  Hour2, Minute2, Second2, Sec1002: word;


function Not_duplicate(var population: mas; i: integer): boolean;
var 
  j: integer;
begin
  Not_duplicate := true;
  for j := 0 to i - 1 do
    if population[j].xord = population[i].xord then
    begin
      Not_duplicate := false;
      exit;
    end;
end;
{--------------------------------------------------------------------------}
procedure Generating_initial_generation(var population: mas; population_volume: integer);
var
  i: integer;
begin
  for i := 0 to population_volume - 1 do
  begin
    repeat
      population[i].xord := Random(65536);
    until Not_duplicate(population, i);
    population[i].x := (population[i].xord * 4) / 65536;
    population[i].y := Main_function(population[i].x);
  end;
end;

{--------------------------------------------------------------------------}
function Stop(iterations, max_iters, valueless_iters, max_valueless_iters: integer; enough_function_value, population_quality: real): boolean;
begin
  Stop := false;
  if iterations = max_iters then
    Stop := true;
  if valueless_iters = max_valueless_iters then
    Stop := true;
  if population_quality >= enough_function_value then  
    Stop := true;
end;
{--------------------------------------------------------------------------}
function Duplicates_exist(var population: mas; population_volume: integer): boolean;
var
  i, j: integer;
begin
  Duplicates_exist := false;
  for i := 0 to population_volume - 2 do
    for j := i + 1 to population_volume - 1 do
    if (population[i].y = population[j].y) and (population[i].x <> -1) and (population[j].x <> -1) then
    begin
      Duplicates_exist := true;
      exit;
    end;
end;
{--------------------------------------------------------------------------}
function Index_of_duplicate(var population: mas; population_volume: integer): integer;
var
  i, j: integer;
  detected: boolean;
begin
  detected := false;
  i := 0;
  j := 1;
  while (not detected) and (i <= (population_volume - 2)) do
  begin
    while (not detected) and (j <= population_volume - 1) do
    begin
      if (population[i].y = population[j].y) and (population[j].x <> -1) then
      begin
        Index_of_duplicate := j;
        detected := true;
      end;
      j := j + 1;
    end;
    i := i + 1;
    j := i + 1;
  end;
end;
{--------------------------------------------------------------------------}
procedure Selection(var population: mas; selection_volume, population_volume, preserved_low_positions, preserved_high_positions: integer);
var
  ind, i, number: integer;
  middle_value, counter, segment, sum: real;
begin
  while Duplicates_exist(population, population_volume) and (selection_volume <> 0) do
  begin
    ind := Index_of_duplicate(population, population_volume);
    {writeln('d ', ind);}
    population[ind].x := -1;
    selection_volume := selection_volume - 1;
  end;
  
  while selection_volume > 0 do
  begin
    number := random(population_volume - preserved_high_positions - preserved_low_positions) + preserved_low_positions;
    if population[number].x <> -1 then  
    begin
      population[number].x := -1;
      selection_volume := selection_volume - 1;
    end;
  end;
end;


procedure Merging(var population, copy_arr: mas; low, mid1, mid2, hight: integer);
var
  i, ind_left, ind_mid, ind_right: integer;
begin
  i := low;
  ind_left := low;
  ind_mid := mid1;
  ind_right := mid2;
  //choose smaller of the smallest in the three ranges
  while (ind_left < mid1) and (ind_mid < mid2) and (ind_right < hight) do
    if copy_arr[ind_left].y > copy_arr[ind_mid].y then
      if copy_arr[ind_left].y > copy_arr[ind_right].y then
      begin
        population[i].y := copy_arr[ind_left].y;
        population[i].x := copy_arr[ind_left].x;
        population[i].xord := copy_arr[ind_left].xord;
        i := i + 1;
        ind_left := ind_left + 1;
      end
      else
      begin
        population[i].y := copy_arr[ind_right].y;
        population[i].x := copy_arr[ind_right].x;
        population[i].xord := copy_arr[ind_right].xord;
        i := i + 1;
        ind_right := ind_right + 1;
      end
    else
      if copy_arr[ind_mid].y > copy_arr[ind_right].y then
      begin
        population[i].y := copy_arr[ind_mid].y;
        population[i].x := copy_arr[ind_mid].x;
        population[i].xord := copy_arr[ind_mid].xord;
        i := i + 1;
        ind_mid := ind_mid + 1;
      end
      else
      begin
        population[i].y := copy_arr[ind_right].y;
        population[i].x := copy_arr[ind_right].x;
        population[i].xord := copy_arr[ind_right].xord;
        i := i + 1;
        ind_right := ind_right + 1;
      end;
  //case where first and second ranges have remaining values
  while (ind_left < mid1) and (ind_mid < mid2) do
    if (copy_arr[ind_left].y > copy_arr[ind_mid].y) then
    begin
      population[i].y := copy_arr[ind_left].y;
      population[i].x := copy_arr[ind_left].x;
      population[i].xord := copy_arr[ind_left].xord;
      i := i + 1;
      ind_left := ind_left + 1;
    end
    else
    begin
      population[i].y := copy_arr[ind_mid].y;
      population[i].x := copy_arr[ind_mid].x;
      population[i].xord := copy_arr[ind_mid].xord;
      i := i + 1;
      ind_mid := ind_mid + 1;
    end;
  //case where second and third ranges have remaining values
  while (ind_mid < mid2) and (ind_right < hight) do
    if (copy_arr[ind_mid].y > copy_arr[ind_right].y) then
    begin
      population[i].y := copy_arr[ind_mid].y;
      population[i].x := copy_arr[ind_mid].x;
      population[i].xord := copy_arr[ind_mid].xord;
      i := i + 1;
      ind_mid := ind_mid + 1;
    end
    else
    begin
      population[i].y := copy_arr[ind_right].y;
      population[i].x := copy_arr[ind_right].x;
      population[i].xord := copy_arr[ind_right].xord;
      i := i + 1;
      ind_right := ind_right + 1;
    end;
  //case where first and third ranges have remaining values
  while (ind_left < mid1) and (ind_right < hight) do
    if (copy_arr[ind_left].y > copy_arr[ind_right].y) then
    begin
      population[i].y := copy_arr[ind_left].y;
      population[i].x := copy_arr[ind_left].x;
      population[i].xord := copy_arr[ind_left].xord;
      i := i + 1;
      ind_left := ind_left + 1;
    end
    else
    begin
      population[i].y := copy_arr[ind_right].y;
      population[i].x := copy_arr[ind_right].x;
      population[i].xord := copy_arr[ind_right].xord;
      i := i + 1;
      ind_right := ind_right + 1;
    end;
  //copy remaining values from the first range
  while ind_left < mid1 do
  begin
    population[i].y := copy_arr[ind_left].y;
    population[i].x := copy_arr[ind_left].x;
    population[i].xord := copy_arr[ind_left].xord;
    i := i + 1;
    ind_left := ind_left + 1;
  end;

  //copy remaining values from the second range
  while ind_mid < mid2 do
  begin
    population[i].y := copy_arr[ind_mid].y;
    population[i].x := copy_arr[ind_mid].x;
    population[i].xord := copy_arr[ind_mid].xord;
    i := i + 1;
    ind_mid := ind_mid + 1;
  end;

  //copy remaining values from the third range
  while ind_right < hight do
  begin
    population[i].y := copy_arr[ind_right].y;
    population[i].x := copy_arr[ind_right].x;
    population[i].xord := copy_arr[ind_right].xord;
    i := i + 1;
    ind_right := ind_right + 1;
  end;
  for i := low to hight - 1 do
  begin
    copy_arr[i].y := population[i].y;
    copy_arr[i].x := population[i].x;
    copy_arr[i].xord := population[i].xord;
  end;
end;

procedure Sorting(var copy_arr, population: mas; low, hight: integer);
var
  mid1, mid2, k: integer;
begin

  if hight - low < 2 then
    exit
  else
  begin
    mid1 := low + ((hight - low) div 3);
    mid2 := low + 2 * ((hight - low) div 3) + 1;
    Sorting(copy_arr, population, mid2, hight);
    Sorting(copy_arr, population, mid1, mid2);
    Sorting(copy_arr, population, low, mid1);


    Merging(population, copy_arr, low, mid1, mid2, hight);
  end;
end;


begin
  GetTime(Hour1, Minute1, Second1, Sec1001);
  //randomize;
  Randseed := 300;
  //Reading the file
  Assign(f, 'file4.txt');
  if not Reading_the_file(f, enough_function_value, max_iters, max_valueless_iters, mutation_type, 
  crossbreeding_type, population_volume, variability, preserved_high_positions, preserved_low_positions, crossing_volume) then
  begin
    writeln('Wrong file');
    exit;
  end;
 
  population_volume := 20;
  max_iters := 100;
  max_valueless_iters := 50;
  enough_function_value := 0.41;
  preserved_high_positions := 2;
  preserved_low_positions := 5;
  crossbreeding_type := 1;
  crossing_volume := 0;
  variability := 4;
  mutation_type := 1;
  
  writeln('Choose operation mode: 1 - testing mode, 2 - main mode');
  repeat
    readln(mode);
  until (mode = 1) or (mode = 2);
  
  //Work with array and values
  SetLength(L, population_volume);
  SetLength(Res, population_volume);
  
  previous_value := -100;
  iterations := 0;
  quality_epsilon := 0;
  
  //Generating the first generation
  Generating_initial_generation(Res, population_volume);
  
  
  //Creatiring copy-array
  for k := 0 to population_volume - 1 do
  begin
    L[k].y := Res[k].y;
    L[k].x := Res[k].x;
    L[k].xord := Res[k].xord;
  end;  
  
  //Sorting
  Sorting(L, Res, 0, population_volume);
  population_quality := Res[0].y;
  //There we have our first population_quality
  
  if mode = 2 then
  begin
    writeln(Res[0].y:4:4);
    while not Stop(iterations, max_iters, valueless_iters, max_valueless_iters, enough_function_value, population_quality) do
    begin
    Selection(Res, variability + crossing_volume, population_volume, preserved_low_positions, preserved_high_positions);
    Crossing(Res, crossbreeding_type, crossing_volume, population_volume);
    Mutation(Res, variability, mutation_type, population_volume);
    
    for k := 0 to population_volume - 1 do
    begin
      L[k].y := Res[k].y;
      L[k].x := Res[k].x;
      L[k].xord := Res[k].xord;
    end;
    
    Sorting(L, Res, 0, population_volume);
    population_quality := Res[0].y;
    
    iterations := iterations + 1; 
    writeln('iteration #', iterations, ' ', Res[0].x:4:4, ' ', Res[0].y:4:4, ' ');
    
    write('-------------------------------');
    writeln();
    if abs(previous_value - population_quality) < quality_epsilon then
      valueless_iters := valueless_iters + 1
    else
      valueless_iters := 0;
    previous_value := population_quality; 
    end;
    GetTime(Hour2 , Minute2 , Second2 , Sec1002);
    writeln('Time of the beginning:');
    writeln(Hour1 ,' : ', Minute1 ,' : ', Second1 ,' : ', Sec1001);
    writeln('Time of the end:');
    writeln(Hour2 ,' : ', Minute2 ,' : ', Second2 ,' : ', Sec1002);
    writeln('Work time (milliseconds):');
    writeln(Sec1002 + Second2*100 + Minute2*6000 + Hour2*360000 -
            Sec1001 - Second1*100 - Minute1*6000 - Hour1*360000);
  end
  else
  begin
    assign(new_file, 'resfile4.txt');
    rewrite(new_file);
    writeln('Do you want population to be written on a screen every interation? yes - 1, no - 2');
    repeat
      readln(onscreen);
    until (onscreen = 1) or (onscreen = 2);
    
    while not Stop(iterations, max_iters, valueless_iters, max_valueless_iters, enough_function_value, population_quality) do
    begin
      Selection(Res, variability + crossing_volume, population_volume, preserved_low_positions, preserved_high_positions);
      Crossing(Res, crossbreeding_type, crossing_volume, population_volume);
      Mutation(Res, variability, mutation_type, population_volume);
      
      for k := 0 to population_volume - 1 do
      begin
        L[k].y := Res[k].y;
        L[k].x := Res[k].x;
        L[k].xord := Res[k].xord;
      end;
      Sorting(L, Res, 0, population_volume);
      population_quality := Res[0].y;
      
      iterations := iterations + 1; 
      writeln(new_file, 'iteration #', iterations, ' ', Res[0].x:4:4, ' ', Res[0].y:4:4, ' ');
      writeln();
      if onscreen = 1 then
        for k := 0 to population_volume - 1 do
        begin
        writeln(Res[k].x:4:4, ' ', Res[k].y:4:4, ' ');
        end;
        
      if abs(previous_value - population_quality) < quality_epsilon then
        valueless_iters := valueless_iters + 1
      else
        valueless_iters := 0;
      previous_value := population_quality;
    end;
    close(new_file);
    writeln(iterations, 'iterations ', Res[0].x:4:4, ' ', Res[0].y:4:4);
  end;
  
  
  
end. 