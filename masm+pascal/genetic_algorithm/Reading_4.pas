﻿unit Reading_4;

interface

function Reading_the_file(var f: text; var p3: real; var p1, p2, p4, p5, p6, p7, p8, p9, p10: integer): boolean;

implementation

function Reading_the_file(var f: text; var p3: real; var p1, p2, p4, p5, p6, p7, p8, p9, p10: integer): boolean;
var
  a: char;
  beginning_of_line, is_number: boolean;
  name, number: string;
  rubbish: integer;
begin
  reset(f);
  Reading_the_file := true;
  while not eof(f) do 
  begin
    beginning_of_line := true;
    is_number := false;
    while not eoln(f) do //reading the line
    begin
      read(f, a);
      if (((a = '#') or (a = ' ')) and beginning_of_line) then
        break
      else
        if (a = ' ') and not beginning_of_line then
          is_number := true
        else
        begin
          beginning_of_line := false;
          if is_number and (a <> '=') and (a <> ' ') then
            number := number + a
          else
            if (a <> '=') and (a <> ' ') then
              name := name + a;
        end;
    end; //the end of reading the line
    case name of
    'max_iters': val(number, p1, rubbish);
    'max_valueless_iters': val(number, p2, rubbish);
    'enough_function_value': val(number, p3, rubbish);
    'mutation_type': val(number, p4, rubbish);
    'crossbreeding_type': val(number, p5, rubbish);
    'population_volume': val(number, p6, rubbish);
    'variability': val(number, p7, rubbish);
    'preserved_high_positions': val(number, p8, rubbish);
    'preserved_low_positions': val(number, p9, rubbish);
    'crossing_volume': val(number, p10, rubbish);
    end;
    {if rubbish <> 0 then 
    begin
      writeln('ruuuur', rubbish);
      Reading_the_file := false;
      p1 := 0;
      p2 := 0;
      p3 := 0;
      p4 := 0;
      p5 := 0;
      p6 := 0;
      exit;
    end;}
    name := '';
    number := '';
    readln(f);
  end;
end;

begin
end.