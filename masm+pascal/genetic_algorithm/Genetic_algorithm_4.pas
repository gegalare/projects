﻿unit Genetic_algorithm_4;

interface

type
  exemplar = record
    x, y: real;
    xord: word;
    coef: integer;
  end;
  mas = array of exemplar;
  copy_mas = array of real;
  
function Main_function(x: real): real;
function Degree(i: integer): word;
procedure Crossing(var population: mas; crossbreeding_type, crossing_volume, population_volume: integer);
procedure Mutation(var population: mas; variability, mutation_type, population_volume: integer);

implementation
{--------------------------------------------------------------------------}
function Main_function(x: real): real;
begin
  Main_function := x * sin(x + 5) * cos(x - 6) * sin(x + 7) * cos(x - 8) * sin(x / 3);
end;
{--------------------------------------------------------------------------}

{--------------------------------------------------------------------------}
function Degree(i: integer): word;
begin
  if i = 0 then
    Degree := 1
  else
    Degree := Degree(i - 1) * 2;  
end;
{--------------------------------------------------------------------------}
procedure Crossing(var population: mas; crossbreeding_type, crossing_volume, population_volume: integer);
var
  t, k, j, i, ex1, ex2, index1, index2: integer;
  mask1, mask2, x1, x2, t1, t2: word;
  flag: boolean;
begin
//Randseed := 30;
mask1 := 0;
mask2 := 0;
k := population_volume - 1;
case crossbreeding_type of
  1 : begin
  for j := 1 to crossing_volume div 2 do 
  begin
    repeat
      ex1 := random(population_volume);
      ex2 := random(population_volume);
    until (population[ex1].x <> -1) and (population[ex2].x <> -1) and (ex1 <> ex2);
    
    index1 := random(15);
    
    //for i := 0 to  index1 do
    //  mask1 := mask1 + Degree(i);
    //mask2 := not mask1;
    
    mask1 := 1;
    t1 := x1 and mask1; 
    t2 := x1 and mask2;
    if t1 <> t2 then
    begin
      x1 := x1 xor mask1;
      x2 := x2 xor mask1;
    end;
    
    mask1 := mask1 shl 1;
    index1 := index1 - 1;
    
    while index1 >=0 do 
    begin
      t1 := x1 and mask1; 
      t2 := x1 and mask2;
      if t1 <> t2 then
      begin
        x1 := x1 xor mask1;
        x2 := x2 xor mask1;
      end;
      mask1 := mask1 shl 1;
      index1 := index1 - 1;
    end;
    
    //x1 := (population[ex1].xord and mask1) or (population[ex2].xord and mask2);
    //x2 := (population[ex2].xord and mask1) or (population[ex1].xord and mask2);
    
    for k := k downto 0 do
    begin
      if population[k].x = -1 then 
      begin
        population[k].xord := x1;
        population[k].x := (population[k].xord * 4) / 65536;
        population[k].y := Main_function(population[k].x);
        break;
      end;  
    end;
    for k := k downto 0 do
    begin
      if population[k].x = -1 then 
      begin
        population[k].xord := x2;
        population[k].x := (population[k].xord * 4) / 65536;
        population[k].y := Main_function(population[k].x);
        break;
      end;  
    end;
   end;
  end;
  2: begin
    for j := 1 to crossing_volume div 2 do 
  begin
    repeat
      ex1 := random(population_volume);
      ex2 := random(population_volume);
    until (population[ex1].x <> -1) and (population[ex2].x <> -1) and (ex1 <> ex2);
    repeat
      index1 := random(15);
      index2 := random(15);
    until index1 <> index2;
    for i := index1 + 1 to  index2 do
      mask1 := mask1 + Degree(i);
    mask2 := not mask1;
    x1 := (population[ex1].xord and mask1) or (population[ex2].xord and mask2);
    x2 := (population[ex2].xord and mask1) or (population[ex1].xord and mask2);
    
    for k := k downto 0 do
    begin
      if population[k].x = -1 then 
      begin
        population[k].xord := x1;
        population[k].x := (population[k].xord * 4) / 65536;
        population[k].y := Main_function(population[k].x);
        break;
      end;  
    end;
    for k := k downto 0 do
    begin
      if population[k].x = -1 then 
      begin
        population[k].xord := x2;
        population[k].x := (population[k].xord * 4) / 65536;
        population[k].y := Main_function(population[k].x);
        break;
      end;  
    end;
   end;
  end;
  3: begin
  for j := 1 to crossing_volume div 2 do 
  begin
  
    repeat
      ex1 := random(population_volume);
      ex2 := random(population_volume);
    until (population[ex1].x <> -1) and (population[ex2].x <> -1) and (ex1 <> ex2);
    {repeat}
      for i := 0 to  15 do
      begin
        t := random(2);
        if t = 1 then
          mask1 := mask1 + random(2) * Degree(i);
      end;
      mask2 := not mask1;
      x1 := (population[ex1].xord and mask1) or (population[ex2].xord and mask2);
      x2 := (population[ex2].xord and mask1) or (population[ex1].xord and mask2);
     
    for k := k downto 0 do
    begin
      if population[k].x = -1 then 
      begin
        population[k].xord := x1;
        population[k].x := (population[k].xord * 4) / 65536;
        population[k].y := Main_function(population[k].x);
        break;
      end;  
    end;
    for k := k downto 0 do
    begin
      if population[k].x = -1 then 
      begin
        population[k].xord := x2;
        population[k].x := (population[k].xord * 4) / 65536;
        population[k].y := Main_function(population[k].x);
        break;
      end;  
    end;
   end;
  end;
  4: begin
    for j := 1 to crossing_volume do 
  begin
    repeat
      ex1 := random(population_volume);
      ex2 := random(population_volume);
    until (population[ex1].x <> -1) and (population[ex2].x <> -1) and (ex1 <> ex2);
    for i := 0 to  15 do begin
      if random(2) = 1 then
        mask1 := mask1 + random(2) * Degree(i);
    end;
    mask2 := not mask1;
    x1 := (population[ex1].xord and mask1) or (population[ex2].xord and mask2);
    for k := population_volume - 1 downto 0 do
    begin
      if population[k].x = -1 then 
      begin
         
        population[k].xord := x1;
        population[k].x := (population[k].xord * 4) / 65536;
        population[k].y := Main_function(population[k].x);
        break;
      end;  
    end;
   end;
  end;
end;

end;
{----------------------------------------------------------------------------}
procedure Mutation(var population: mas; variability, mutation_type, population_volume: integer);
var
  temp, k, i, j, ex, bit1, bit2: integer;
  temp_ex, mask1, mask2, new_ex: word;
begin
  //Randseed := 30;
  mask1 := 0;
  mask2 := 0;
  k := population_volume - 1;
  case mutation_type of
  1:
  begin
    for i := 1 to variability do
    begin
      repeat
        ex := random(population_volume);
      until population[ex].x <> -1;
      
      j := random(16);
      mask1 := 1;
      mask1 := mask1 shl j;
      for k := k downto 0 do 
        if population[k].x = -1 then
        begin
          population[k].xord := (population[ex].xord xor mask1);
          population[k].x := (population[k].xord * 4) / 65536;
          population[k].y := Main_function(population[k].x);
          break;
        end;
    end;
  end;
  2: 
  begin
    for i := 1 to variability do
    begin
      repeat
        ex := random(population_volume);
      until population[ex].x <> -1;
      repeat
        bit1 := random(16);
        bit2 := random(16);
      until bit1 <> bit2;
      if bit1 > bit2 then
      begin
        temp := bit1;
        bit1 := bit2;
        bit2 := temp;
      end;
      mask1 := Degree(bit1);
      mask2 := Degree(bit2);
      temp_ex := population[ex].xord and mask2; //right bit
      new_ex := (population[ex].xord and (not mask2)) or ((population[ex].xord and mask1) shl(bit2 - bit1));
      new_ex := (population[ex].xord and (not mask1)) or (temp_ex shr(bit2 - bit1));
      for k := k downto 0 do 
        if population[k].x = -1 then
        begin
          population[k].xord := new_ex;
          population[k].x := (population[k].xord * 4) / 65536;
          population[k].y := Main_function(population[k].x);
          break;
        end;
    end;
  end;
  3: 
  begin
    for i := 1 to variability do
    begin
      repeat
        ex := random(population_volume);
      until population[ex].x <> -1;
     bit1 := random(16);
     for j := 0 to bit1 do
      new_ex := (ex and (65535 xor Degree(bit1 - j))) or (ex and Degree(j)); 
     for k := k downto 0 do 
        if population[k].x = -1 then
        begin
          population[k].xord := new_ex;
          population[k].x := (population[k].xord * 4) / 65536;
          population[k].y := Main_function(population[k].x);
          break;
        end;
    end;
  end;
  end;
end;

begin
end.