.NOLIST
.NOLISTMACRO
.386
.model flat, stdcall
.code
;---------------------CROSSING--------------------------------------
public FIRST_CROSSING
FIRST_CROSSING proc
;�������� ����  
        push ebp
        mov ebp, esp
        push eax
        push ebx
        push ecx
        push edx
        push esi
        push edi
;�������� ����� �� �������    
        mov ax, [ebp + 20]
        mov bx, [ebp + 24]
;��������� �����   
        mov cx, 1
        mov si, [ebp + 8]
;� ����� �������� ���������� ��� �����     
Process:
        mov dx, cx
        and dx, ax
        mov di, cx
        and di, bx
        cmp dx, di
        je End_process
        xor ax, cx
        xor bx, cx
;� ����� ������ �����, ��������� ��������� ���    
End_process:
        shl cx, 1
        dec si
        cmp si, 0
        jge Process
        mov ecx, [ebp + 12]
        mov [ecx], bx
        mov ecx, [ebp + 16]
        mov [ecx], ax
        pop edi
        pop esi
        pop edx
        pop ecx
        pop ebx
        pop eax
        pop ebp
        ret 20
FIRST_CROSSING endp


public SECOND_CROSSING
SECOND_CROSSING proc
;�������� ����    
        push ebp
        mov ebp, esp

        push eax
        push ebx
        push ecx
        push edx
        push esi
        push edi

        mov ax, [ebp + 24]
        mov bx, [ebp + 28]
;����� ������� �� ������� �������  
        mov cx, 1
        mov dx, [ebp + 12]
;���������, �� �������� �� �����   
        cmp dx , 0
        je Not_shift
        mov di, dx
Shift:
        shl cx, 1
        dec di
        cmp di, 0
        jg Shift
Not_shift:    
        mov si, [ebp + 8]
        sub si, dx
;��������� ��������� ������     
Process:
        mov dx, cx
        and dx, ax
        mov di, cx
        and di, bx
        cmp dx, di
        je Next_step
        xor ax, cx
        xor bx, cx
;����� �� ��������� �������    
Next_step:
        shl cx, 1
        dec si
        cmp si, 0
        jge Process
        mov ecx , [ebp + 16]
        mov [ecx], ax
        mov ecx, [ebp + 20]
        mov [ecx], bx
        pop edi
        pop esi
        pop edx
        pop ecx
        pop ebx
        pop eax
        pop ebp
        ret 24 
SECOND_CROSSING endp



public THIRD_CROSSING
THIRD_CROSSING proc

        push ebp
        mov ebp, esp

        push eax
        push ebx
        push ecx
        push edx
        push esi
        push edi

        mov ax, [ebp + 20]
        mov bx, [ebp + 24]

;������ �����    
        mov cx, 1
        mov si, [ebp + 8]
     
Process:
;��������� �� ���� ��� � �����
        mov dx, si
        and dx, cx
        cmp dx, 0
        jne Next_step
;��������� ���������
        mov dx, cx
        and dx, ax
        mov di, cx
        and di, bx
        cmp dx, di
        je Next_step

        xor ax, cx
        xor bx, cx
    
Next_step:
;����� �����
        shl cx, 1
        cmp cx, 0
        jne Process

        mov ecx, [ebp + 12]
        mov [ecx], ax
        mov ecx, [ebp + 16]
        mov [ecx], bx

        pop edi
        pop esi
        pop edx
        pop ecx
        pop ebx
        pop eax
        pop ebp
        ret 20
    
THIRD_CROSSING endp


public FOURTH_CROSSING
FOURTH_CROSSING proc
    
        push ebp
        mov ebp, esp

        push eax
        push ebx
        push ecx
        push edx
        push esi
        push edi

        mov ax, [ebp + 20]
        mov bx, [ebp + 16]
;������ �����    
        mov cx, 1
        mov si, [ebp + 8]
     
Process:
;��������� ��� ����� �� ����
        mov dx, si
        and dx, cx
        cmp dx, 0
        jne Next
;�o������� ���������
        mov dx, cx
        and dx, ax
        mov di, cx
        and di, bx
        cmp dx, di
        je Next
        xor ax, cx
    
Next:
;����� - ��������� ������� ������
        shl cx , 1
        cmp cx , 0
        jne Process
        mov ecx, [ebp + 12]
        mov [ecx], ax

        pop edi
        pop esi
        pop edx
        pop ecx
        pop ebx
        pop eax
        pop ebp
        ret 16
    
FOURTH_CROSSING endp


;------------------------------MUTATIONS----------------------------------


public FIRST_MUTATION
FIRST_MUTATION proc
    
        push ebp
        mov ebp, esp

        push eax
        push ebx
        push ecx

        mov ax, [ebp + 16]

;������ �����
        mov cx, 1
        mov bx, [ebp + 8]
;�������� �� ����    
        cmp bx , 0
        je End_shift

Shift:
        shl cx, 1
        dec bx
        cmp bx , 0
        jg Shift

End_shift:    
;������ ������ ����    
        xor ax, cx

        mov ecx, [ebp + 12]
        mov [ecx] , ax

        pop ecx
        pop ebx
        pop eax
        pop ebp
        ret 12
FIRST_MUTATION endp

;procedure     Swap     (normal :word; 
;                        var invalid :word;
;                        mask1 , mask2 :integer);
;Stack:
;   + 8     mask_2
;   + 12    mask_1
;   + 16    invalid
;   + 20    normal

public SECOND_MUTATION
SECOND_MUTATION proc

        push ebp
        mov ebp, esp

        push eax
        push ebx
        push ecx
        push edx
        push esi
        push edi
        mov ax, [ebp + 20]
;������ �����
        mov cx, 1
        mov bx, [ebp + 8]
;�������� �� ������� �����
        cmp bx, 0
        je End_shift_1

Shift_1:
        shl cx, 1
        dec bx
        cmp bx, 0 
        jg Shift_1

End_shift_1:    
;������ ����� 
        mov dx, 1
        mov bx, [ebp + 12]
;�������� �� ����
        cmp bx, 0
        je End_shift_2

Shift_2:
        shl dx, 1
        dec bx
        cmp bx, 0
        jg Shift_2

End_shift_2:    
;������ ������� ����
        mov si, cx
        and si, ax
        mov di, dx
        and di, ax
        cmp si, di
        je Result
        xor ax, cx
        xor ax, dx

Result:    
        mov ecx, [ebp + 16]
        mov [ecx], ax
        pop edi
        pop esi
        pop edx
        pop ecx
        pop ebx
        pop eax
        pop ebp
        ret 16
    
SECOND_MUTATION endp

;procedure    Reverse   (normal :word; 
;                        var invalid :word;
;                        mask :integer);
;Stack:
;   + 8     mask
;   + 12    invalid
;   + 16    normal

public THIRD_MUTATION
THIRD_MUTATION proc
    
        push ebp 
        mov ebp, esp

        push eax
        push ebx
        push ecx
        push edx
        push esi
        push edi
        mov ax, [ebp + 16]

;������ �����
        mov cx, 1
        mov bx, [ebp + 8]
;�������� �� ����
        cmp bx, 0
        je Stop_shift

Shift:
        shl cx, 1
        dec bx
        cmp bx, 0 
        jg Shift

Stop_shift:    
        mov bx, [ebp + 8]
        mov dx, 1

Cycle:
;�������� �� ���������� ����
        mov si, cx
        and si, ax
        mov di, dx
        and di, ax

        cmp si, 0
        je Zero

        cmp di, 0
        je Continue_not_equal
        jmp Continue_equal
    
Zero:
        cmp di, 0
        je Continue_equal
        jmp Continue_not_equal

Continue_not_equal:    
        xor ax, cx

Continue_equal:
        shl dx, 1
        shr cx, 1
        dec bx
        cmp bx, 0
        jge Cycle
            
        mov ecx, [ebp + 12]
        mov [ecx], ax

        pop edi
        pop esi
        pop edx
        pop ecx
        pop ebx
        pop eax
        pop ebp
        ret 12
    
THIRD_MUTATION endp


end