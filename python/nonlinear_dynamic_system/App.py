from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QLabel, QLineEdit, QRadioButton, QVBoxLayout, QFrame, QPushButton
from PyQt5.QtGui import QPixmap
from PyQt5.Qt import Qt
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import sys
import os
import VAN

def resource_path(relative_path):
     if hasattr(sys, '_MEIPASS'):
         return os.path.join(sys._MEIPASS, relative_path)
     return os.path.join(os.path.abspath("."), relative_path)

class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Практикум по ЭВМ. Гладков Глеб, гр. 304.')
        self.setGeometry(100, 100, 1100, 510)
        self.setFixedWidth(1100)
        self.setFixedHeight(510)

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.ax1 = self.figure.add_subplot(121)
        self.ax2 = self.figure.add_subplot(122)
        self.plot_widget = QWidget(self)
        self.plot_widget.setGeometry(280, 10, 800, 400)
        plot_box = QVBoxLayout()
        plot_box.addWidget(self.canvas)
        self.plot_widget.setLayout(plot_box)

        self.label0 = QLabel("<b>Задача М2.3", self)
        self.label0.move(20, 10)

        self.label9 = QLabel("<b>Осциллятор Ван-Дер-Поля", self)
        self.label9.setGeometry(20, 30, 200, 20)

        self.label8 = QLabel("<b>Параметры: ", self)
        self.label8.move(20, 60)

        self.label1 = QLabel("t      =", self)
        self.label1.move(20, 90)
        self.lineEdit1 = QLineEdit(self)
        self.lineEdit1.setGeometry(80, 90, 50, 30)
        self.lineEdit1.setText("100")

        self.label2 = QLabel("dt     =", self)
        self.label2.move(20, 130)
        self.lineEdit2 = QLineEdit(self)
        self.lineEdit2.setGeometry(80, 130, 50, 30)
        self.lineEdit2.setText("0.01")

        self.label3 = QLabel("eps1 =", self)
        self.label3.move(160, 130)
        self.lineEdit3 = QLineEdit(self)
        self.lineEdit3.setGeometry(200, 130, 50, 30)
        self.lineEdit3.setText("0.0001")

        self.label4 = QLabel("E    =", self)
        self.label4.move(160, 90)
        self.lineEdit4 = QLineEdit(self)
        self.lineEdit4.setGeometry(200, 90, 50, 30)
        self.lineEdit4.setText("0.15")

        self.label5 = QLabel("eps2 =", self)
        self.label5.move(160, 170)
        self.lineEdit5 = QLineEdit(self)
        self.lineEdit5.setGeometry(200, 170, 50, 30)
        self.lineEdit5.setText("0.0001")

        self.label7 = QLabel("", self)
        self.label7.setGeometry(750, 445, 400, 30)
        
        self.label_error = QLabel("", self)
        self.label_error.setGeometry(370, 200, 400, 30)
        
        self.button = QPushButton('Посчитать и нарисовать', self)
        self.button.setGeometry(20, 445, 250, 40)
        self.button.clicked.connect(self.plot)
        
        self.label6 = QLabel("<b>Примеры: ", self)
        self.label6.setGeometry(20, 190, 200, 30)
        
        self.image = QLabel(self)
        pixmap = QPixmap(resource_path("Van_Der_Pol.png")).scaledToHeight(100, Qt.SmoothTransformation)
        self.image.setGeometry(290, 405, 600, 100)
        self.image.setPixmap(pixmap)

        self.ex1 = QPushButton('Неусточивый узел 1', self)
        self.ex1.setGeometry(20, 220, 250, 25)
        self.ex1.clicked.connect(lambda: self.example(1))

        self.ex2 = QPushButton('Неусточивый узел 2', self)
        self.ex2.setGeometry(20, 245, 250, 25)
        self.ex2.clicked.connect(lambda: self.example(2))

        self.ex3 = QPushButton('Неусточивый узел 3', self)
        self.ex3.setGeometry(20, 270, 250, 25)
        self.ex3.clicked.connect(lambda: self.example(3))

        self.ex4 = QPushButton('Неусточивый узел 4', self)
        self.ex4.setGeometry(20, 295, 250, 25)
        self.ex4.clicked.connect(lambda: self.example(4))

        self.ex5 = QPushButton('Неусточивый фокус 1', self)
        self.ex5.setGeometry(20, 320, 250, 25)
        self.ex5.clicked.connect(lambda: self.example(5))

        self.ex6 = QPushButton('Неусточивый фокус 2', self)
        self.ex6.setGeometry(20, 345, 250, 25)
        self.ex6.clicked.connect(lambda: self.example(6))

        self.ex7 = QPushButton('Неусточивый фокус 3', self)
        self.ex7.setGeometry(20, 370, 250, 25)
        self.ex7.clicked.connect(lambda: self.example(7))

        self.ex8 = QPushButton('Неусточивый фокус 4', self)
        self.ex8.setGeometry(20, 395, 250, 25)
        self.ex8.clicked.connect(lambda: self.example(8))
        
        self.plot()
        self.show()
    

    def example(self, n):
        if n == 1:
            self.lineEdit1.setText("500")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.0001")
            self.lineEdit4.setText("0.01")
            self.lineEdit5.setText("0.001")

        if n == 2:
            self.lineEdit1.setText("50")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.0001")
            self.lineEdit4.setText("0.25")

        if n == 3:
            self.lineEdit1.setText("500")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.0001")
            self.lineEdit4.setText("0.25")
            self.lineEdit5.setText("0.001")

        if n == 4:
            self.lineEdit1.setText("50")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.0001")
            self.lineEdit4.setText("0.5")
            self.lineEdit5.setText("0.001")

        if n == 5:
            self.lineEdit1.setText("50")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.0001")
            self.lineEdit4.setText("0.55")

        if n == 6:
            self.lineEdit1.setText("50")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.0001")
            self.lineEdit4.setText("0.8")
            self.lineEdit5.setText("0.001")

        if n == 7:
            self.lineEdit1.setText("50")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.0001")
            self.lineEdit4.setText("1.0")
            self.lineEdit5.setText("0.001")

        if n == 8:
            self.lineEdit1.setText("20")
            self.lineEdit2.setText("0.1")
            self.lineEdit3.setText("0.001")
            self.lineEdit4.setText("1.0")
            self.lineEdit5.setText("0.001")
            
    def plot(self):
        self.ax1.clear()
        self.ax2.clear()
        self.label_error.setText("")
        
        if len(self.lineEdit1.text())!=0:
            self.t = float(self.lineEdit1.text())
        else:
            self.t = 250
            self.lineEdit1.setText("250")

        if len(self.lineEdit2.text()) != 0:
            self.dt = float(self.lineEdit2.text())
        else:
            self.dt = 0.01
            self.lineEdit2.setText("0.01")

        if len(self.lineEdit3.text()) != 0:
            self.eps1 = float(self.lineEdit3.text())
            if self.eps1 <= 0.0 or self.eps1 >= 0.1:
                self.eps1 = 0.01
                self.lineEdit3.setText("0.01")
        else:
            self.eps1 = 0.01
            self.lineEdit3.setText("0.01")

        if len(self.lineEdit5.text()) != 0:
            self.eps2 = float(self.lineEdit5.text())
            if self.eps2 <= 0.0 or self.eps2 >= 0.1:
                self.eps2 = 0.01
                self.lineEdit3.setText("0.01")
        else:
            self.eps2 = 0.01
            self.lineEdit5.setText("0.01")

        if len(self.lineEdit4.text()) != 0:
            self.E = float(self.lineEdit4.text())
        else:
            self.E = 0.1
            self.lineEdit4.setText("0.1")
        
        try:
            response = VAN.thing(self.ax1, self.ax2, self.E, self.t, self.dt, self.eps1, self.eps2)
            self.label7.setText(response)
        except Exception as e:
            self.label_error.setText(str(e))
        
        plt.tight_layout()
        plt.gcf().subplots_adjust(bottom=0.12, top=0.9, wspace=0.25)
        self.canvas.draw()
        
if __name__=='__main__':
    app = QApplication(sys.argv)
    w = Window()
    app.exec_()
