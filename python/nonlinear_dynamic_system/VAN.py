import numpy as np 
from scipy.integrate import odeint
import math 

def step(state, t, E, R):
    y1, y2 = state
    return (y2 - (y1**3) / 3 + y1) / E, -E * y1

def thing(ax1, ax2, E, t_stop = 200, dt = 0.01, eps1 = 0.01, eps2 = 0.01):
    
    iters = int(t_stop / dt)
    t = np.linspace(0, t_stop, iters)
    R = E

    start_point_1 = 0.0
    start_point_2 = 0.0
    state = [start_point_1 + eps1, start_point_2 + eps2]

    solution = odeint(step, state, t, args = (E, R))
    
    response = 'Особая точка ({:.1f}; {:.1f}) — '.format(start_point_1, start_point_2)
    if E > 0.5:
        response += 'Неустойчивый фокус' 
    elif E <= 0.5:
        response += 'Неусточивый узел'
    
    ax1.plot(t, solution[:, 0], 'r', label='y1(t)')
    ax1.plot(t, solution[:, 1], 'g', label='y2(t)')
    ax1.legend()
    ax1.set_ylim(bottom = -2.2)
    ax1.set_xlim(left = -2.2)
    ax1.grid()
    ax1.set(xlabel = 't')
    
    
    line = ax2.plot(solution[:, 0], solution[:, 1])[0]
    ax2.set_ylim(bottom = -2.5)
    ax2.set_xlim(left = -2.5)
    ax2.grid(linewidth = 1)
    ax2.set(xlabel = 'y1', ylabel = 'y2')
    
    return response