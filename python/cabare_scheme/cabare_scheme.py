import matplotlib.pyplot as plt
import numpy as np
import math


def u_start(x):
    return (2 * math.sin(math.pi * x / 4) * math.exp(-1 * math.log(2) * (x / 3)**2))

def w_start(x):
    return (math.sin(math.pi * x / 4) * math.exp(-1 * math.log(2) * (x / 3)**2))

def u_analize(x, time):
    return ((math.sin(math.pi * (x-time) / 4) * math.exp(-1 * math.log(2) * ((x-time) / 3)**2)) + 3*(math.sin(math.pi * (x-3*time) / 4) * math.exp(-1 * math.log(2) * ((x-3*time)/ 3)**2)))/2

def w_analize(x, time):
    return (3*(math.sin(math.pi * (x-3*time) / 4) * math.exp(-1 * math.log(2) * ((x-3*time) / 3)**2)) - (math.sin(math.pi * (x-time) / 4) * math.exp(-1 * math.log(2) * ((x-time)/ 3)**2)))/2




lambda_max = 3.0
t = 12.5
x = 25.0
x1 = -25.0

step = int(input("Введите размерность сетки: "))
CFL  = float(input("Введите CFL: "))
h    = (x-x1) / step
tau  = CFL * h / 3
#mu   = h**2 - 3 * lambda_max * tau * h + 2 * lambda_max**2 * tau**2


A = np.array([[2, 1], [1, 2]])
#s_v_Matrix = np.array([[1, 1], [1, -1]])
#lambda_Matrix = np.array([[1, 0], [0, 3]])
#dispers_Matrix = np.array([[mu, 0], [0, mu]])
L = np.array([[1, -1], [1, 1]])
L_inv = np.linalg.inv(L)
#print(L_inv)

u = []
w = []
u_centre = []
w_centre = []


k = x1
j = 0.0

while k <= x:
    u.append(u_start(k))
    w.append(w_start(k))
    k += h / 2

k = x1 + h/2
while k <= (x - h / 2):
    u_centre.append(0.0)
    w_centre.append(0.0)
    k += h

k = 0.0
while k < t-tau:
    j = 0.0
    i = 0
    while j <= (x-x1-h):
        u_temp = -1 * tau * (u[i+2] - u[i]) / h / 2
        w_temp = -1 * tau * (w[i+2] - w[i]) / h / 2
        U_temp = np.array([[u_temp], [w_temp]])
        A_temp = A.dot(U_temp)
        u_temp = (2*A_temp[0][0] - A_temp[1][0])/(3)
        w_temp = (2*A_temp[1][0] - A_temp[0][0])/(3)
        u_centre[int(i / 2)] = (u_temp + u[i+1])
        w_centre[int(i / 2)] = (w_temp + w[i+1])
        j += h
        i += 2

    j = 0.0
    i = 0
    while j <= x-x1:
        if j == 0.0:
            u_temp_centre = u_centre[-1]
            w_temp_centre = w_centre[-1]
            u_temp = u[-3]
            w_temp = w[-3]
        else:

            u_temp_centre = u_centre[int(i / 2) - 1]
            u_temp = u_save
            w_temp_centre = w_centre[int(i / 2) - 1]
            w_temp = w_save
            
        R_centre = np.array([[u_temp_centre], [w_temp_centre]])
        R = np.array([[u_temp], [w_temp]])
        R_temp_centre = L.dot(R_centre)
        R_temp = L.dot(R)
        R_1_main = 2 * R_temp_centre[0][0] - R_temp[0][0]
        R_2_main = 2 * R_temp_centre[1][0] - R_temp[1][0]
        R = np.array([[R_1_main], [R_2_main]])
        R_use = L_inv.dot(R)
        u_save = u[i]
        w_save = w[i]
        u[i] = (R_use[0][0] - R_use[1][0])/2
        w[i] = (R_use[1][0] - R_use[0][0])/2
        j += h
        i += 2

    j = 0.0
    i = 0
    while j < (x-x1-h):
        u_temp = -1 * (u[i+2] - u[i]) / h * tau / 2
        w_temp = -1 * (w[i+2] - w[i]) / h * tau / 2
        U_temp = np.array([[u_temp], [w_temp]])
        A_temp = A.dot(U_temp)
        u_temp = (2 * A_temp[0][0] - A_temp[1][0])/(3)
        w_temp = (2 * A_temp[1][0] - A_temp[0][0])/(3)
        u[i + 1] = (u_temp + u_centre[int(i / 2)])
        w[i + 1] = (w_temp + w_centre[int(i / 2)])
        
        j += h
        i += 2
    k += tau

m = 0.0
i = int((len(u)-1)/2)
#i = 0
x_m = []
u_y = []
w_y = []
u_anal_y = []
w_anal_y = []
while m <= x:
    x_m.append(m)
    m = m + h
while i <= (2*step):
    u_y.append(u[i])
    w_y.append(w[i])
    i = i + 2

i = 0
while i <= (step/2):
    u_anal_y.append(u_analize(x_m[i], t))
    w_anal_y.append(w_analize(x_m[i], t))
    i = i + 1

plt.plot(x_m, u_anal_y, 'v-.g', alpha = 0.5, label = "u_a(x,t)", lw = 1.5, ms = 3)
plt.plot(x_m, u_y, 'o-r', alpha = 0.5, label = "u(x,t)", lw = 1.5, ms = 3)
plt.xlabel("x")
plt.legend()
plt.grid(True)
plt.show()
max = 0
for i in range(len(u_y)):
    if abs(u_anal_y[i] - u_y[i]) > max:
        max = abs(u_anal_y[i] - u_y[i])
    elif abs(w_anal_y[i] - w_y[i]) > max:
        max = abs(u_anal_y[i] - u_y[i])
plt.plot(x_m, w_anal_y, 'o-r', alpha = 0.5, label = "u_a(x,t)", lw = 1.5, ms = 3)
plt.plot(x_m, w_y, 'v-.g', alpha = 0.5, label = "u(x,t)", lw = 1.5, ms = 3) 
plt.xlabel("x")
plt.legend()
plt.grid(True)
plt.show()
print("Погрешность по C-норме: ", max)
print("CFL: ", CFL)
q = input()



