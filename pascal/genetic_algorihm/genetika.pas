Program Genetika;  
Uses Crt, mutation, scresh;                          
Type parametri = record 
                    max_iters: integer; 
                    population_volume: integer; 
                    max_valueless_iters: integer;
                    rejim: integer; 
                    screshivanie: integer;
                    mutaciya: integer;
                    quality_epsilon: real;
                    enough_function_value: real;
                    preserved_high_position: integer;
                    preserved_low_position: integer;
                    max, argmax: real;
                    Dannie_vvod: text; 
                    dannie_vivod: text;
                 end;

    func = record
                koef: real;
                x: real;
                y: real;
           end;

Var Algoritm, krit_ost: parametri;
    gen, copy_gen: array of func;
    l, na_ekran: integer;
    Best_znach: func;

function Vvod (var Config: text; var alg: parametri): boolean;
var symb: char;
    i: integer;
begin
    vvod := true;
    i := 0;
    Repeat
        repeat
            read(Config, symb);
            if symb = '#' then readln(config)
            else repeat
                    read(Config, symb);
                 until symb = '=';
        until symb = '=';
        read(config,symb);
        i := i + 1;
        case i of
            1: begin   
                    read(Config, Alg.rejim);  
                    if (alg.rejim <> 1) and (alg.rejim <> 2) then
                    begin
                        writeln('oshibka');
                        exit;
                    end;
                end;
            2: begin 
                    read(Config, Alg.screshivanie);
                    if (Alg.screshivanie <> 1) and (Alg.screshivanie <> 2) 
                        and (Alg.screshivanie <> 3) and (Alg.screshivanie <> 4) then
                    begin
                        writeln('oshibka');
                        exit;
                    end;
                end;
            3: begin read(Config, Alg.mutaciya);
                    if (Alg.mutaciya <> 1) and (Alg.mutaciya <> 2) 
                        and (Alg.mutaciya <> 3) then
                    begin
                        writeln('oshibka');
                        exit;
                    end;
                end;
            4: read(Config, Alg.max_iters);
            5: read(config, Alg.population_volume);
            6: read(config, Alg.quality_epsilon);
            7: read(config, Alg.max_valueless_iters);
            8: read(config, Alg.enough_function_value);
            9: read(config, Alg.preserved_high_position);
            10: read(config, Alg.preserved_low_position);
        end;
        if i < 10 then
            readln(config);
    Until i >= 10;
end;

function F (y: real): real;  
begin
    F := y * sin(y + 5) * cos(y - 6) * sin(y + 7) * cos(y - 8) * sin(y / 3);
end;

procedure Generaciya;
var k: integer;
begin
    randomize;
    for k := 0 to Algoritm.population_volume - 1 do 
    begin
        gen[k].koef := random(65535); 
    end;
end;

procedure Kachestvo;
var i: integer;
begin
    for i := 0 to Algoritm.population_volume - 1 do
    begin
        Gen[i].x := Gen[i].koef * 4 / 65536;
        Gen[i].y := F(Gen[i].koef * 4 / 65536);
    end;
end;

procedure Selekciya;
var x1, x2, pobeditel: integer;
begin
    randomize;
    x1 := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    x2 := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    while x2 = 1 do
        x2 := random(algoritm.population_volume - 1);
    pobeditel := random(1);
    if pobeditel = 0 then
        gen[x2].koef := random(65535)
    else gen[x1].koef := random(65535);
end;

procedure Merge(var main_arr, copy_arr: array of func; nach_mas, mid1, mid2, konec_mas: integer);
var
    i, lev_gran, sered_gran, prav_gran: integer;
begin
    i := nach_mas;
    lev_gran := nach_mas;
    sered_gran := mid1;
    prav_gran := mid2;
    while (lev_gran < mid1) and (sered_gran < mid2) and (prav_gran < konec_mas) do
        if  copy_arr[lev_gran].y >  copy_arr[sered_gran].y then 
            if  copy_arr[lev_gran].y >  copy_arr[prav_gran].y then
            begin
                main_arr[i].y := copy_arr[lev_gran].y;
                main_arr[i].x := copy_arr[lev_gran].x;
                main_arr[i].koef := copy_arr[lev_gran].koef;
                i := i + 1;
                lev_gran := lev_gran + 1;
             end
            else
            begin
                main_arr[i].y := copy_arr[prav_gran].y;
                main_arr[i].x := copy_arr[prav_gran].x;
                main_arr[i].koef := copy_arr[prav_gran].koef;
                i := i + 1;
                prav_gran := prav_gran + 1;
            end
        else
        if  copy_arr[sered_gran].y >  copy_arr[prav_gran].y then
            begin
                main_arr[i].y := copy_arr[sered_gran].y;
                main_arr[i].x := copy_arr[sered_gran].x;
                main_arr[i].koef := copy_arr[sered_gran].koef;
                i := i + 1;
                sered_gran := sered_gran + 1;
            end  
            else
            begin
                main_arr[i].y := copy_arr[prav_gran].y;
                main_arr[i].x := copy_arr[prav_gran].x;
                main_arr[i].koef := copy_arr[prav_gran].koef;
                i := i + 1;
                prav_gran := prav_gran + 1;
            end; 
    while (lev_gran < mid1) and (sered_gran < mid2) do
        if ( copy_arr[lev_gran].y >  copy_arr[sered_gran].y) then 
        begin  
            main_arr[i].y := copy_arr[lev_gran].y;
            main_arr[i].x := copy_arr[lev_gran].x;
            main_arr[i].koef := copy_arr[lev_gran].koef;
            i := i + 1;
            lev_gran := lev_gran + 1;
        end
        else
        begin
            main_arr[i].y := copy_arr[sered_gran].y;
            main_arr[i].x := copy_arr[sered_gran].x;
            main_arr[i].koef := copy_arr[sered_gran].koef;
            i := i + 1;
            sered_gran := sered_gran + 1;
        end;
    while (sered_gran < mid2) and (prav_gran < konec_mas) do
        if ( copy_arr[sered_gran].y >  copy_arr[prav_gran].y) then 
        begin  
            main_arr[i].y := copy_arr[sered_gran].y;
            main_arr[i].x := copy_arr[sered_gran].x;
            main_arr[i].koef := copy_arr[sered_gran].koef;
            i := i + 1;
            sered_gran := sered_gran + 1;
        end
        else
        begin
            main_arr[i].y := copy_arr[prav_gran].y;
            main_arr[i].x := copy_arr[prav_gran].x;
            main_arr[i].koef := copy_arr[prav_gran].koef;
            i := i + 1;
            prav_gran := prav_gran + 1;
        end;
    while (lev_gran < mid1) and (prav_gran < konec_mas) do
        if ( copy_arr[lev_gran].y >  copy_arr[prav_gran].y) then 
        begin  
            main_arr[i].y := copy_arr[lev_gran].y;
            main_arr[i].x := copy_arr[lev_gran].x;
            main_arr[i].koef := copy_arr[lev_gran].koef;
            i := i + 1;
            lev_gran := lev_gran + 1;
        end
        else
        begin
            main_arr[i].y := copy_arr[prav_gran].y;
            main_arr[i].x := copy_arr[prav_gran].x;
            main_arr[i].koef := copy_arr[prav_gran].koef;
            i := i + 1;
            prav_gran := prav_gran + 1;
        end;
    while lev_gran < mid1 do 
    begin
        main_arr[i].y := copy_arr[lev_gran].y;
        main_arr[i].x := copy_arr[lev_gran].x;
        main_arr[i].koef := copy_arr[lev_gran].koef;
        i := i + 1;
        lev_gran := lev_gran + 1;
    end;
    while sered_gran < mid2 do 
    begin
        main_arr[i].y := copy_arr[sered_gran].y;
        main_arr[i].x := copy_arr[sered_gran].x;
        main_arr[i].koef := copy_arr[sered_gran].koef;
        i := i + 1;
        sered_gran := sered_gran + 1;
    end; 

    while prav_gran < konec_mas do 
    begin
        main_arr[i].y := copy_arr[prav_gran].y;
        main_arr[i].x := copy_arr[prav_gran].x;
        main_arr[i].koef := copy_arr[prav_gran].koef;
        i := i + 1;
        prav_gran := prav_gran + 1;
    end;
    for i := nach_mas to konec_mas - 1 do 
    begin
        copy_arr[i].y := main_arr[i].y;
        copy_arr[i].x := main_arr[i].x;
        copy_arr[i].koef := main_arr[i].koef;
    end;
end;

procedure Sortirovka(var  copy_arr, main_arr: array of func; nach_mas, konec_mas: integer);
var
  mid1, mid2: integer;
begin
    if konec_mas - nach_mas < 2 then
    exit
    else
    begin
        mid1 := nach_mas + ((konec_mas - nach_mas) div 3);
        mid2 := nach_mas + 2 * ((konec_mas - nach_mas) div 3) + 1;
        sortirovka(copy_arr, main_arr, mid2, konec_mas);
        sortirovka(copy_arr, main_arr, mid1, mid2);
        sortirovka(copy_arr, main_arr, nach_mas, mid1);
        Merge(main_arr,  copy_arr, nach_mas, mid1, mid2, konec_mas);
    end;
end;

function Kriteriy_ostanova: byte;
begin
    if krit_ost.max_iters >= Algoritm.max_iters then
        Kriteriy_ostanova := 1;
    if krit_ost.enough_function_value >= Algoritm.enough_function_value then 
        Kriteriy_ostanova := 1;
    if krit_ost.max_valueless_iters >= Algoritm.max_valueless_iters - 1 then
        Kriteriy_ostanova := 1
end;

procedure Mutation_1_1; //изменение случайно выбранного бита
var
    a, x1: longint;
begin
    a := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    x1 := round(gen[a].x * 10000);
    a := random(15) + 1;
    Mutation_1(x1, a);
    gen[algoritm.population_volume - 1].x := x1 / 10000;
    gen[algoritm.population_volume - 1].y := F(gen[algoritm.population_volume - 1].x);
end;
    
procedure Mutation_2_1; //перестановка случайно выбранных битов местами
var
    a, b, x1: longint;
begin
    a := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    x1 := round(gen[a].x * 10000);
    a := random(15) + 1;
    repeat
        b := random(15) + 1;
    until b <> a;
    Mutation_2(x1, a, b);
    gen[algoritm.population_volume - 1].x := x1 / 10000;
    gen[algoritm.population_volume - 1].y := F(gen[algoritm.population_volume - 1].x);
end;
    
procedure Mutation_3_1; //реверс битовой строки, начиная со случайно выбранного бита
var
    a, x1: longint;
begin
    a := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    x1 := round(gen[a].x * 10000);
    a := random(15) + 1;
    Mutation_3(x1, a);
    gen[algoritm.population_volume - 1].x := x1 / 10000;
    gen[algoritm.population_volume - 1].y := F(gen[algoritm.population_volume - 1].x);
end;

procedure scresh_1;
var
    a, b, x1, x2: longint;
begin
    a := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    b := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    if a = b then
        b := (a + random(algoritm.population_volume - algoritm.preserved_high_position - 
            algoritm.preserved_low_position) + algoritm.preserved_low_position + 1 mod 
            (algoritm.population_volume));
            
    x1 := round(gen[a].x * 10000);
    x2 := round(gen[b].x * 10000);
    a := random(15) + 1;
    scresh.scresh_1(x1, x2, a);
    gen[algoritm.population_volume - 2].x := x1 / 10000;
    gen[algoritm.population_volume - 2].y := F(gen[algoritm.population_volume - 2].x);
    gen[algoritm.population_volume - 1].x := x2 / 10000;
    gen[algoritm.population_volume - 1].y := F(gen[algoritm.population_volume - 1].x);
end;

procedure scresh_2;
var
    a, a1, b, x1, x2: longint;
begin
    a := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    b := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    if a = b then
        b := (a + random(algoritm.population_volume - algoritm.preserved_high_position - 
            algoritm.preserved_low_position) + algoritm.preserved_low_position + 1 mod 
            (algoritm.population_volume)); 
            
    x1 := round(gen[a].x * 10000);
    x2 := round(gen[b].x * 10000);
    a := random(15) + 1;
    repeat
        a1 := random(15) + 1;
    until a1 <> a;
    scresh.scresh_2(x1, x2, a, a1);
    gen[algoritm.population_volume - 2].x := x1 / 10000;
    gen[algoritm.population_volume - 2].y := F(gen[algoritm.population_volume - 2].x);
    gen[algoritm.population_volume - 1].x := x2 / 10000;
    gen[algoritm.population_volume - 1].y := F(gen[algoritm.population_volume - 1].x);
end;
    
procedure scresh_3;
var
    a, b, x1, x2: longint;
begin
    a := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    b := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    if a = b then
        b := (a + random(algoritm.population_volume - algoritm.preserved_high_position - 
            algoritm.preserved_low_position) + algoritm.preserved_low_position + 1 mod 
            (algoritm.population_volume));
            
    x1 := round(gen[a].x * 10000);
    x2 := round(gen[b].x * 10000);
    scresh.scresh_3(x1, x2);
    gen[algoritm.population_volume - 2].x := x1 / 10000;
    gen[algoritm.population_volume - 2].y := F(gen[algoritm.population_volume - 2].x);
    gen[algoritm.population_volume - 1].x := x2 / 10000;
    gen[algoritm.population_volume - 1].y := F(gen[algoritm.population_volume - 1].x);
end;
    
procedure scresh_4;
var
    a, b, x1, x2, mask1, mask2: longint;
begin
    a := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    b := random(algoritm.population_volume - algoritm.preserved_high_position - 
        algoritm.preserved_low_position - 1) + algoritm.preserved_low_position + 1;
    if a = b then
        b := (a + random(algoritm.population_volume - algoritm.preserved_high_position - 
            algoritm.preserved_low_position) + algoritm.preserved_low_position + 1 mod 
            (algoritm.population_volume));
            
    x1 := round(gen[a].x * 10000);
    x2 := round(gen[b].x * 10000);
    mask1 := random(65535);
    repeat
        mask2 := random(65535);
    until mask1 <> mask2;
    scresh.scresh_4(x1, x2, mask1, mask2);
    gen[algoritm.population_volume - 2].x := x1 / 10000;
    gen[algoritm.population_volume - 2].y := F(gen[algoritm.population_volume - 2].x);
    gen[algoritm.population_volume - 1].x := x2 / 10000;
    gen[algoritm.population_volume - 1].y := F(gen[algoritm.population_volume - 1].x);
end;

begin
    best_znach.y := 0;
    best_znach.x := 0;
    best_znach.koef := 0;
    krit_ost.max_iters := 0;
    krit_ost.max_valueless_iters := 1;
    assign(algoritm.dannie_vivod, 'Vivod.txt');
    rewrite(algoritm.dannie_vivod);
    assign(Algoritm.Dannie_vvod, 'file.txt');
    reset(Algoritm.Dannie_vvod);
    Vvod(Algoritm.Dannie_vvod, Algoritm);
    close(Algoritm.Dannie_vvod);
    setLength(gen, Algoritm.population_volume);
    setLength(copy_gen, Algoritm.population_volume);
    Generaciya;
    if algoritm.rejim = 1 then
    begin 
        writeln('jelaete vivesti poshagovoe vipolnenie programmi na ekran? Da - 1, Net - 2');
        read(na_ekran);
    end;
    while Kriteriy_ostanova <> 1 do 
    begin
        Kachestvo;
        Sortirovka(gen, copy_gen, 0, Algoritm.population_volume);
        Selekciya;
        case Algoritm.screshivanie of
            1: scresh_1;
            2: scresh_2;
            3: scresh_3;
            4: scresh_4;
        end;
        case algoritm.mutaciya of
            1: Mutation_1_1;
            2: Mutation_2_1;
            3: Mutation_3_1;
        end;
        for l := 0 to algoritm.population_volume - 1 do
        begin
            copy_gen[l].x := 0;
            copy_gen[l].y := 0;
            copy_gen[l].koef := 0;
        end;
        Sortirovka(gen, copy_gen, 0, Algoritm.population_volume);
        if gen[1].y <> best_znach.y then
            krit_ost.max_valueless_iters := 0;
        if gen[1].y = best_znach.y then
            krit_ost.max_valueless_iters := krit_ost.max_valueless_iters + 1;
        If best_znach.y < gen[1].y then
        begin
            best_znach.y := gen[1].y;
            best_znach.x := gen[1].x;
        end;
        krit_ost.enough_function_value := gen[1].y;
        krit_ost.max_iters := krit_ost.max_iters + 1;
        
        if na_ekran = 1 then
        begin
            writeln;
            writeln('Population: ', krit_ost.max_iters);
            for l := 0 to algoritm.population_volume - 1 do
                begin
                    writeln(' x=', gen[l].x:8:8, ' y=', gen[l].y:8:8);
                end;
        end;
        if algoritm.rejim = 2 then 
        begin
            writeln('Popul=', krit_ost.max_iters,' best_x=', gen[1].x:8:8, 'best_y=', gen[1].y:8:8);
        end;
        for l := 0 to algoritm.population_volume - 1 do
             writeln(algoritm.dannie_vivod,
                     'Popul=', krit_ost.max_iters,
                     ' best_x=', gen[l].x:8:8,
                     ' best_y=', gen[l].y:8:8
                    );
    end;
    writeln(algoritm.dannie_vivod,'Best of the best, best_x=', best_znach.x:8:8,
            ' best_y=', best_znach.y:8:8);
    writeln('Best of the best, best_x=', best_znach.x:8:8,
            ' best_y=', best_znach.y:8:8);

    close(algoritm.dannie_vivod);
end.
