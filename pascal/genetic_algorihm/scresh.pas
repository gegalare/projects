Unit Scresh;

Interface
    procedure scresh_1(var x1, x2: longint; a: longint);
    procedure scresh_2(var x1, x2, a, a1: longint);
    procedure scresh_3(var x1, x2: longint);
    procedure scresh_4(var x1, x2, mask1, mask2: longint);

Implementation
    procedure scresh_1(var x1, x2: longint; a: longint);
    var
        mask1, mask2, t1, t2, i: longint;
    begin
        mask1 := 1 shl (16 - a);
        t1 := x1 and mask1;
        t2 := x2 and mask1;
        if t1 <> 0 then
            x2 := x2 or mask1
        else
        begin
            mask2 := not mask1;
            for i := 1 to (a - 1) do
                mask2 := mask2 or (1 shl (16 - i));
            x2 := x2 and mask2;
        end;
        if t2 <> 0 then
            x1 := x1 or mask1
        else
        begin
            mask2 := not mask1;
            for i := 1 to (a - 1) do
                mask2 := mask2 or (1 shl (16 - i));
            x1 := x1 and mask2;
        end;
    end;
    
    procedure scresh_2(var x1, x2, a, a1: longint);
    var
        mask1, mask2, t1, t2, i: longint;
    begin
        mask1 := 1 shl (16 - a);
        t1 := x1 and mask1;
        t2 := x2 and mask1;
        if t1 <> 0 then
            x2 := x2 or mask1
        else
        begin
            mask2 := not mask1;
            for i := 1 to (a - 1) do
                mask2 := mask2 or (1 shl (16 - i));
            x2 := x2 and mask2;
        end;
        if t2 <> 0 then
            x1 := x1 or mask1
        else
        begin
            mask2 := not mask1;
            for i := 1 to (a - 1) do
                mask2 := mask2 or (1 shl (16 - i));
            x1 := x1 and mask2;
        end;
        a := a1;
        mask1 := 1 shl (16 - a);
        t1 := x1 and mask1;
        t2 := x2 and mask1;
        if t1 <> 0 then
            x2 := x2 or mask1
        else
        begin
            mask2 := not mask1;
            for i := 1 to (a - 1) do
                mask2 := mask2 or (1 shl (16 - i));
            x2 := x2 and mask2;
        end;
        if t2 <> 0 then
            x1 := x1 or mask1
        else
        begin
            mask2 := not mask1;
            for i := 1 to (a - 1) do
                mask2 := mask2 or (1 shl (16 - i));
            x1 := x1 and mask2;
        end;
    end;
    
    procedure scresh_3(var x1, x2: longint);
    var
        i, a: longint;
    begin
        for i := 1 to 11 do
        begin
            a := random(2);
            if a = 0 then
                continue;
            scresh_1(x1, x2, i);
        end;
    end;
    
    procedure scresh_4(var x1, x2, mask1, mask2: longint);
    var
        i, m, t, xr1, xr2: longint;
    begin
        xr1 := 0;
        xr2 := 0;
        for i := 1 to 16do
        begin
            m := 1 shl (16 - i);
            t := m and mask1;
            if t <> 0 then
                xr1 := xr1 + x1 and m
            else
                xr1 := xr1 + x2 and m;
        end;
        for i := 1 to 16do
        begin
            m := 1 shl (16 - i);
            t := m and mask2;
            if t <> 0 then
                xr2 := xr2 + x1 and m
            else
                xr2 := xr2 + x2 and m;
        end;
        x1 := xr1;
        x2 := xr2;
    end;
end.