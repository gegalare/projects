Unit mutation;

Interface
    procedure Mutation_1(var x1, a: longint);
    procedure Mutation_2(var x1, a, b: longint);
    procedure Mutation_3(var x1, a: longint);

Implementation
    procedure Mutation_1(var x1, a: longint);
    var
        i, mask1: longint;
    begin
        mask1 := 1 shl (16 - a);
        if (x1 and mask1) = 0 then
            x1 := x1 or mask1
        else
        begin
            mask1 := not mask1;
            for i := 1 to (a - 1) do
                mask1 := mask1 or (1 shl (16 - i));
            x1 := x1 and mask1;
        end;
    end;
    
    procedure Mutation_2(var x1, a, b: longint);
    var
        i, mask1, mask2, t1, t2: longint;
    begin
        mask1 := 1 shl (16 - a);
        mask2 := 1 shl (16 - b);
        t1 := x1 and mask1;
        t2 := x1 and mask2;
        if t1 <> 0 then
            x1 := x1 or mask2
        else
        begin
            mask2 := not mask2;
            for i := 1 to (16 - b) do
                mask2 := mask2 or (1 shl (16 - i));
            x1 := x1 and mask2;
        end;
        if t2 <> 0 then
            x1 := x1 or mask1
        else
        begin
            mask1 := not mask1;
            for i := 1 to (16 - a) do
                mask1 := mask1 or (1 shl (16 - i));
            x1 := x1 and mask1;
        end;
    end;
    
    procedure Mutation_3(var x1, a: longint);
    var
        i, mask, xr, t: longint;
    begin
        xr := 0;
        for i := 1 to (16 - a + 1) do
        begin
            mask := 1 shl (i - 1);
            t := mask and x1;
            if t <> 0 then
                xr := xr + 1 shl (16 - a + 1 - i);
        end;
        mask := 0;
        for i := 1 to (a - 1) do
            mask := mask + 1 shl (16 - i);
        mask := x1 and mask;
        x1 := xr or mask; 
    end;
end.