include console.inc

N equ 512

.const
		Hello           db '������' , 0
        Vvod_1          db '������� ������ ����� ' , 0
        Vvod_2          db '������� ������ ����� ' , 0
        mode_1          db '������ ����� ����� ������� ��������������: ' , 0
        mode_2          db '������ ����� ����� ������� ��������������: ' , 0
        mode_3          db '������ ����� ����� ������� ��������������: ' , 0
        mode_4          db '������ ����� ����� ������� ��������������: ' , 0
        Error           db '����� ���� ������, ���� ������� �������' , 0
        New_string      db '������� ������: '
.data
        Text1       db N dup (0)
        Text2       db N dup (0)
		Mas1        db N dup (0)
		Mas2        db N dup (0)
        Length1     dd 0
        Length2     dd 0
.code

;______________������ ������� �����______________
;
; ������� ����� ��� ���� � ������ � �������
; �������� ��������� ���������� �������.
;
len              equ [ebp - 4]
stop_flag        equ [ebp - 8]
string_counter   equ [ebp - 12]
symbol_counter   equ [ebp - 16]
slash_flag       equ [ebp - 20]
main_len         equ [ebp - 24]
temp             equ [ebp - 24]
        
InputText proc

		push        ebp
		mov         ebp , esp
		sub         esp , 28
		push        ecx
		push        ebx
		push        edi
		push        esi

		mov         dword ptr [ebp - 4] , 0
		mov         dword ptr [ebp - 8] , 0
		mov         dword ptr [ebp - 12] , 0
		mov         dword ptr [ebp - 16] , 0
		mov         dword ptr [ebp - 20] , 0
		mov         dword ptr [ebp - 24] , 0
		mov         dword ptr [ebp - 28] , 0

		

		xor         edx , edx ;������ � ������� ������ �������
		mov         edx , [ebp + 12] ;������ �� ��������� ������
		xor         ecx , ecx
		mov         ecx , [ebp + 8] ;������ �� �������� ������

;______________������ �������� �����______________

Cycle_main:
		mov         dword ptr symbol_counter , 0
        xor         ebx , ebx
        mov         edi , edx
        SetTextAttr Cyan
        outstrln    offset New_string
        inputstr    edx , N,
        SetTextAttr
        cmp         eax , 0
        je          End_of_main_cycle
    
;______________������ ����������� �����______________

Cycle_1:
		cmp         dword ptr slash_flag , 1
		je          Not_slash ;�������, ���� �� ����
        cmp         byte ptr [edi] , '\'
        jne         Not_slash
        mov         dword ptr slash_flag , 1
        jmp         Slash

Not_slash:
        cmp         byte ptr [edi] ,'@'
        jne         End_of_cycle_1
        cmp         byte ptr [edi + 1] , '%'
        jne         End_of_cycle_1
        cmp         byte ptr [edi + 2] , '%'
        jne         End_of_cycle_1
        cmp         byte ptr [edi + 3] , '@'
        jne         End_of_cycle_1
        cmp         dword ptr slash_flag , 1
        je          End_of_cycle_1
        jmp         Stop_input

Stop_input:
		cmp         dword ptr symbol_counter , 0
		je          Go_out
		add         dword ptr string_counter , 1

Go_out:
		jmp         Out_label


End_of_cycle_1:
		mov         dword ptr temp, ebx ;���������� ������� � �������� �����
		xor         ebx , ebx
		mov         bl , [edi]
		mov         byte ptr [ecx] , bl
		add         ecx , 1
		mov         ebx , dword ptr temp
        add         dword ptr len , 1   ;���������� ���������
        add         dword ptr symbol_counter , 1
        mov         dword ptr slash_flag , 0

Slash:
        add         edi , 1
        sub         eax , 1
        cmp         eax , 1
        jae         Cycle_1

;______________����� ����������� �����______________

End_of_main_cycle:
		mov         dword ptr symbol_counter , 0
		mov         byte ptr [ecx] , 13
		add         ecx , 1
		mov         byte ptr [ecx] , 10
		add         ecx , 1
		mov         dword ptr slash_flag , 0
		add         dword ptr string_counter , 1
        add         edx, eax
        add         edx , 2
        add         dword ptr len , 2
        cmp         dword ptr len , N
        ja          ERROR
        jb          Cycle_main

ERROR:
		mov         eax, 0
		jmp         Clear

;______________����� �������� �����______________

Out_label:
        mov         edx, string_counter
        cmp         dword ptr string_counter, 0
        je          ERROR
        cmp         dword ptr len , N
        ja          ERROR
        mov         eax , 1

Clear:
		pop         esi
		pop         edi
		pop         ebx
		pop         ecx
		add         esp , 28
		pop         ebp
		ret         12
InputText endp

;______________����� ������� �����______________


;______________������ ������ ���������______________

First_change proc

		push        ebp
		mov         ebp , esp
		push        ebx
		push        ecx
		mov         ebx , [ebp + 8]
		dec         ebx
		xor         esi , esi

Next_letter:
		cmp         esi , N
		jge         End_first_change

		inc         ebx
		inc         esi
		mov         dl , byte ptr [ebx]
		cmp         byte ptr [ebx] , 'A'
		jl          Next_letter
		cmp         byte ptr [ebx] , 'Z'
		jg          Second_check
		add         dl , 32
		mov         byte ptr [ebx] , dl
		jmp         Next_letter

Second_check:
		cmp         byte ptr [ebx] , 'a'
		jl          Next_letter
		cmp         byte ptr [ebx] , 'z'
		jg          Next_letter
		sub         dl , 32
		mov         byte ptr [ebx] , dl
		jmp         Next_letter

End_first_change:
		pop         ecx
		pop         ebx
		pop         ebp
		ret         8

First_change endp

;______________����� ������ ���������______________


;______________������ ������ ���������______________

Second_change proc

        push        ebp
        mov         ebp , esp
        push        ecx
        push        ebx
        push        edx

        xor         edi , edi
		mov         ecx , N ;ecx:=����� ������� N
        mov         edx , [ebp+8]
        mov         ebx , [ebp+8]
        add         edx , ecx
        add         ebx , ecx
		dec         ebx
		dec         edx

Comp:
        mov         al , [ebx]
		cmp         al , 128
		jb          Next
		cmp         al , 159
		ja          Next
		inc         edi

Next:
        dec         ebx
		cmp         ebx , [ebp + 8]
		jae         Comp
		add         ebx , ecx
		add         edx , edi

proc2:
        mov         al , [ebx]
        mov         [edx] , al
		cmp         al , 128
		jb          Next1
		cmp         al , 159
		ja      	Next1
        mov         [edx-1] , al
        dec         edx

Next1:
        dec         edx
        dec         ecx
        dec         ebx
        cmp         ecx , 0
        ja      	proc2
		xor         edi , edi

		pop         edx
        pop         ebx
        pop         ecx
        pop         ebp
        ret         8

Second_change endp

;______________����� ������ ���������______________



;_______________������ �������� ���������______________

Start:
        clrscr
        SetTextAttr White
        outstrln    offset Hello
		outstrln    offset Vvod_1
		push        offset Mas1
		push        offset Text1
		call        InputText
		cmp         eax , 0
		je          Ne_text
		mov         Length1 , edx
        SetTextAttr White
        outstrln
                
		outstrln    offset Vvod_2
		push        offset Mas2
		push        offset Text2
		call        InputText
		cmp         eax , 0
		je          Ne_text
		mov         Length2 , edx
		jmp         Changing

Ne_text:
        SetTextAttr Red
        outstrln
		outstrln    offset Error
		SetTextAttr
		exit

Changing:
		mov         eax , Length1
		mov         ebx , Length2
		cmp         eax , ebx
		ja          Perviy
		jmp         Vtoroy
Perviy:
        outstrln
        SetTextAttr Yellow
		push        offset Text1
		call        First_change
        outstrln    offset mode_1
		ConsoleMode ;�������� ������������� � DOS
		outstrln    '"""'
		outstrln    offset Text1
		outstrln    '"""'
		ConsoleMode ;��������� ������������� � DOS

		push        offset Text2
		call        Second_change
        outstrln
        outstrln    offset mode_2
        ConsoleMode ;�������� ������������� � DOS
		outstrln    '"""'
		outstrln    offset Text2
		outstrln    '"""'
        SetTextAttr
		exit
Vtoroy:
        outstrln
        SetTextAttr Yellow
		push        offset Text2
		call        First_change
        outstrln    offset mode_3
		ConsoleMode ;�������� ������������� � DOS
		outstrln    '"""'
		outstrln    offset Text2
		outstrln    '"""'
        ConsoleMode ;��������� ������������� � DOS

        push        offset Text1
		call        Second_change
        outstrln
        outstrln    offset mode_4
		ConsoleMode ;�������� ������������� � DOS
		outstrln    '"""'
		outstrln    offset Text1
		outstrln    '"""'
        SetTextAttr
		exit
end Start


